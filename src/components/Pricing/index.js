import React, { useState } from 'react';
import Icon from '@material-ui/core/Icon';

import Button from '../../components/Button';
import Border from '../../assets/img/group-7@2x.png';
import Circle2 from '../../assets/img/group-2-copy-2.png';
import Circle1 from '../../assets/img/group-2.png';
import Circle3 from '../../assets/img/group-2-copy.png';
import Circle4 from '../../assets/img/group-2-copy-3.png';
import BgImage from '../../assets/img/bg-space@3x.png';

import './style.css';

function Pricing() {

  return (
    <div className="pricing-container" style={{backgroundImage: `url(${BgImage})`}}>
      <div className="border-chats-container">
        <div className="border-chats-circle" style={{backgroundImage: `url(${Circle1})`}}></div>
        <div>
          <div className="border-chats" style={{backgroundImage: `url(${Border})`}}>
            “We basically consider Vidnetics to be our source of truth for finding monetized videos on Youtube. If it’s
            not in Vidnetcis, I don’t trust."
          </div>
          <div className="pricing-container-people-info">
            <div className="pricing-container-people-img"></div>
            <div className="pricing-container-people-info-work">
              <div className="bold-position">Marketing Director</div>
              <div>at Starbucks</div>
            </div>
          </div>
        </div>
        <div className="border-chats-circle border-chats-circle-bottom"
             style={{backgroundImage: `url(${Circle2})`}}></div>
      </div>
      <div>
        <div className="pricing-container-btn-container">
          <Button className="monthly-btn">Monthly</Button>
          <Button className="yearly-btn">Yearly</Button>
        </div>
        <div className="pricing-container-content">
          <div className="border-chats-circle border-chats-circle-3" style={{backgroundImage: `url(${Circle3})`}}></div>
          <div className="border-chats-circle border-chats-circle-4" style={{backgroundImage: `url(${Circle4})`}}></div>
          <div className="rate-container">
            <h1 className="rate-header">FREE</h1>
            <div className="rate-count"><span className="currency">$</span>O/mo</div>
            <p className="rate-desc">Get started, find monetized videos and create your first video ad for free.</p>
            <div className="rate-checklist">
              <div><Icon className="active">check_circle</Icon>Render in full HD</div>
              <div><Icon className="active">check_circle</Icon>Upload directly into YT</div>
              <div><Icon className="active">check_circle</Icon>Create your own tamplate</div>
              <div><Icon>check_circle</Icon>1 video ad.</div>
              <div><Icon>check_circle</Icon>10 video ad.</div>
              <div><Icon>check_circle</Icon>Unlimited video ads.</div>
            </div>

            <Button className="pricing-btn-get-started">GET STARTED</Button>
          </div>
          <div className="rate-container">
            <div className="sale-label">20% OFF</div>
            <h1 className="rate-header">Optimal</h1>
            <div className="rate-count"><span className="currency">$</span>29/mo</div>
            <p className="rate-desc">Get started, find monetized videos and create your first video ad for free.</p>
            <div className="rate-checklist">
              <div><Icon className="active">check_circle</Icon>Render in full HD</div>
              <div><Icon className="active">check_circle</Icon>Upload directly into YT</div>
              <div><Icon className="active">check_circle</Icon>Create your own tamplate</div>
              <div><Icon className="active">check_circle</Icon>1 video ad.</div>
              <div><Icon className="active">check_circle</Icon>10 video ad.</div>
              <div><Icon>check_circle</Icon>Unlimited video ads.</div>
            </div>

            <Button className="pricing-btn-get-started">GET STARTED</Button>
          </div>
          <div className="rate-container">
            <h1 className="rate-header">Premium</h1>
            <div className="rate-count"><span className="currency">$</span>69/mo</div>
            <p className="rate-desc">Get started, find monetized videos and create your first video ad for free.</p>
            <div className="rate-checklist">
              <div><Icon className="active">check_circle</Icon>Render in full HD</div>
              <div><Icon className="active">check_circle</Icon>Upload directly into YT</div>
              <div><Icon className="active">check_circle</Icon>Create your own tamplate</div>
              <div><Icon className="active">check_circle</Icon>1 video ad.</div>
              <div><Icon className="active">check_circle</Icon>10 video ad.</div>
              <div><Icon className="active">check_circle</Icon>Unlimited video ads.</div>
            </div>

            <Button className="pricing-btn-get-started">GET STARTED</Button>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Pricing;