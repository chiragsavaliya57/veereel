import React, { useState } from 'react';

import { useDispatch, useSelector } from 'react-redux';
import { findVideo } from '../../reducer/videoInfo/actions';

import Button from '../../components/Button';
import Tabs from '../../components/Tabs';
import Input from '../../components/Input';
import CustomSelect from '../../components/CustomSelect';
import Icon from '@material-ui/core/Icon';
import DateTimePicker from '../../components/DateTimePicker';
import ComingSoon from '../../components/ComingSoon';

import './style.css';


function VideoSearch() {
  const tabData = [
    {
      tabName: <div className="video-search-tab-title">By Keywords</div>,
      tabContent: <SearchByKeyword/>,
    },
    {
      tabName: <div className="video-search-tab-title">By Relation to Video</div>,
      tabContent: <ComingSoon/>,
    },
    {
      tabName: <div className="video-search-tab-title">By Channel</div>,
      tabContent: <ComingSoon/>,
    },
  ];

  return (
    <div className="video-search-container">
      <div className="video-search-title-container">
        <h1 className="video-search-title">Search for Monetized Videos</h1>
        <Button
          onClick={() => {
          }}
          className="clear-search-button"
        >
          Clear results
        </Button>
      </div>
      <div className="video-search-content-container">
        <Tabs tabData={tabData}/>
      </div>
    </div>
  );
}

function SearchByKeyword() {
  const dispatch = useDispatch();
  const [infoByKeyword, setInfoByKeyword] = useState({
    searchKeyword: null,
    neededNumber: 0,
    // lang: 1,
    // priority: 1,
    // publishedBefore: null,
    // publishedAfter: null,
  });

  const [advanceSearch, setAdvanceSearch] = useState(false);
  const options = [...Array(100).keys()].map((i) => i + 1);

  const findVideos = () => {
    const info = {
      searchKeyword: infoByKeyword.searchKeyword,
      neededNumber: options[infoByKeyword.neededNumber],
    };
    dispatch(findVideo(info));
  };

  return (
    <div className="search-by-kw-container">
      <div className="search-by-kw-container-row">
        <Input
          value={infoByKeyword.searchKeyword}
          className="search-by-kw-input"
          labelText="Keywords"
          onChange={(e) => {
            setInfoByKeyword({...infoByKeyword, searchKeyword: e});
          }}
        />
        <CustomSelect
          options={options}
          handleChange={(e) => {
            setInfoByKeyword({...infoByKeyword, neededNumber: e});
          }}
          value={infoByKeyword.neededNumber}
          label='Max Results'
          multiple={false}
          className="search-by-kw-select"
        />
      </div>

      <div className="search-by-kw-container-row advance-container">
        <div onClick={() => setAdvanceSearch(!advanceSearch)}>Advance search {advanceSearch ?
          <Icon>arrow_drop_up</Icon> : <Icon>arrow_drop_down</Icon>}</div>
      </div>
      {/*{advanceSearch &&*/}
      {/*<div>*/}
      {/*<div className="search-by-kw-container-row advance-params-container">*/}
      {/*<CustomSelect*/}
      {/*options={options}*/}
      {/*handleChange={(e) => {*/}
      {/*setInfoByKeyword({...infoByKeyword, lang: e});*/}
      {/*}}*/}
      {/*value={infoByKeyword.lang}*/}
      {/*label='Language'*/}
      {/*multiple={false}*/}
      {/*className="search-by-kw-lang-select"*/}
      {/*/>*/}
      {/*<CustomSelect*/}
      {/*options={options}*/}
      {/*handleChange={(e) => {*/}
      {/*setInfoByKeyword({...infoByKeyword, priority: e});*/}
      {/*}}*/}
      {/*value={infoByKeyword.priority}*/}
      {/*label='Search Priority'*/}
      {/*multiple={false}*/}
      {/*className="search-by-kw-search-priority-select"*/}
      {/*/>*/}
      {/*</div>*/}
      {/*<div className="search-by-kw-container-row advance-params-container">*/}
      {/*<DateTimePicker*/}
      {/*closeOnSelect={true}*/}
      {/*value={infoByKeyword.publishedBefore}*/}
      {/*label="Published before"*/}
      {/*placeholder="Year/month/day"*/}
      {/*className="search-by-kw-datetime"*/}
      {/*handleChange={(e) => {*/}
      {/*console.log(e);*/}
      {/*}}*/}
      {/*/>*/}
      {/*<DateTimePicker*/}
      {/*closeOnSelect={true}*/}
      {/*label="Published After"*/}
      {/*value={infoByKeyword.publishedAfter}*/}
      {/*className="search-by-kw-datetime"*/}
      {/*placeholder="Year/month/day"*/}
      {/*handleChange={(e) => {*/}
      {/*console.log(e);*/}
      {/*}}/>*/}
      {/*</div>*/}
      {/*</div>}*/}
      <div className="search-btn-container">
        <Button
          onClick={findVideos}
          className="search-by-kw-btn"
        >
          <Icon>search</Icon>Search
        </Button>
      </div>
    </div>
  );
}

export default VideoSearch;