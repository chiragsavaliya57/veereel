import React, {useEffect} from "react";
import {useDispatch, useSelector} from 'react-redux';
import logo from "../../assets/img/small-logo.png"

import Sidebar from "../../assets/jss/material-dashboard-pro-react/components/Sidebar/Sidebar";
import UserInfo from './UserInfo'

import { getUserInfo } from '../../reducer/user/actions';
import "./style.css";

function CustomSidebar({routes, handleDrawerToggle, color, bgColor, miniActive,open, props, logoText}) {
  const dispatch = useDispatch();
  const {payload: payloadAuth } = useSelector(state => state.auth.login);
  const {payload: payloadUserInfo} = useSelector(state => state.user.info);

  useEffect(()=>{
    if (!payloadUserInfo) {
      dispatch(getUserInfo());
    }
  }, [payloadAuth]);

  return (
    <div className="custom-sidebar-container">
      <Sidebar
        routes={routes}
        logoText={logoText}
        logo={logo}
        handleDrawerToggle={handleDrawerToggle}
        open={open}
        color={color}
        bgColor={bgColor}
        miniActive={miniActive}
        user={<UserInfo />}
        mainPageLink={""}
        {...props}
      />
    </div>
  );
}

export default  React.memo(CustomSidebar);
