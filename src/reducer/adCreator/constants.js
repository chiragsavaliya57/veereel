export const GET_AD_VIDEO = 'get-ad-video/start';
export const GET_AD_VIDEO_SUCCESS = 'get-ad-video/success';
export const GET_AD_VIDEO_FAILED = 'get-ad-video/failed';

export const GET_AD_VIDEO_TASKS = 'get-ad-video-tasks/start';
export const GET_AD_VIDEO_TASKS_SUCCESS = 'get-ad-video-tasks/success';
export const GET_AD_VIDEO_TASKS_FAILED = 'get-ad-video-tasks/failed';

export const UPLOAD_AD_VIDEO_IMAGE = 'upload-ad-video-image/start';
export const UPLOAD_AD_VIDEO_IMAGE_SUCCESS = 'upload-ad-video-image/success';
export const UPLOAD_AD_VIDEO_IMAGE_FAILED = 'upload-ad-video-image/failed';

export const UPLOAD_AD_VIDEO_SOUND = 'upload-ad-video-sound/start';
export const UPLOAD_AD_VIDEO_SOUND_SUCCESS = 'upload-ad-video-sound/success';
export const UPLOAD_AD_VIDEO_SOUND_FAILED = 'upload-ad-video-sound/failed';

export const POST_SESSION = 'post-session/start';
export const POST_SESSION_SUCCESS = 'post-session/success';
export const POST_SESSION_FAILED = 'post-session/failed';

export const GET_SESSION2 = 'get-session2/start';
export const GET_SESSION_SUCCESS = 'get-session/success';
export const GET_SESSION_FAILED = 'get-session/failed';

export const POST_VIDEO = 'post-video/start';
export const POST_VIDEO_SUCCESS = 'post-video/success';
export const POST_VIDEO_FAILED = 'post-video/failed';

export const GET_VIDEO = 'get-video/start';
export const GET_VIDEO_SUCCESS = 'get-video/success';
export const GET_VIDEO_FAILED = 'get-video/failed';

export const GET_SCRIPT_SUGGESTION = 'get-script-suggestion/start';
export const GET_SCRIPT_SUGGESTION_SUCCESS = 'get-script-suggestion/success';
export const GET_SCRIPT_SUGGESTION_FAILED = 'get-script-suggestion/failed';

export const POST_NOTIFICATION = 'post-notification/start';
export const POST_NOTIFICATION_SUCCESS = 'post-notification/success';
export const POST_NOTIFICATION_FAILED = 'post-notification/failed';

export const GET_NOTIFICATION = 'get-notification/start';
export const GET_NOTIFICATION_SUCCESS = 'get-notification/success';
export const GET_NOTIFICATION_FAILED = 'get-notification/failed';

export const DELETE_NOTIFICATION = 'delete-notification/start';
export const DELETE_NOTIFICATION_SUCCESS = 'delete-notification/success';
export const DELETE_NOTIFICATION_FAILED = 'delete-notification/failed';
