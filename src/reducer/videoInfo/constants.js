export const GET_VIDEO = 'get-video/start';
export const GET_VIDEO_SUCCESS = 'get-video/success';
export const GET_VIDEO_FAILED = 'get-video/failed';

export const FIND_VIDEO = 'find-video';
export const FIND_VIDEO_SUCCES = 'find-video/success';
export const FIND_VIDEO_FAILED = 'find-video/failed';