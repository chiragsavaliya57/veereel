import React, { useState, useEffect } from 'react';
import Button from '../Button';
import Icon from '@material-ui/core/Icon';

import Input from '../Input';
import Tooltips from '../Tooltip';
import CustomSelect from "../CustomSelect"


function PaymentMethod() {
  const [isChangeCard, setChangeCard] = useState(false);
  const [userPaymentInfo, setUserPaymentInfo] = useState({
    name: 'Tikhon',
    lastName: 'B',
    cardNumber: '**** **** **** 1234',
    month: null,
    year: null,
    cvc: null,
    country: {
      value: null,
      name: null
    },
    city: null,
    address: null,
    state: null,
    zip: null
  });

  const countries = ["Russia", "USA", "UK"];

  const handleChangeInputs = (type, e) => {
    setUserPaymentInfo({...userPaymentInfo, [type]: e});
  };


  return <div className="billing-container">
    <h1 className="billing-container-title">Payment method</h1>
    {isChangeCard ?
      <div className="billing-container-content billing-container-column-content update-payment-method">
        <div className="billing-container-column-row row-names">
          <Input
            type="text" value={userPaymentInfo.name}
            onChange={(e) => handleChangeInputs('name', e)}
            placeholder={'Enter first name'}
            labelText={'First Name'}
          />
          <Input
            type="text" value={userPaymentInfo.lastName}
            onChange={(e) => handleChangeInputs('lastName', e)}
            placeholder={'Enter last Name'}
            labelText={'Last Name'}
          />
        </div>
        <div className="billing-container-column-row row-card-info">
          <Input
            type="text"
            mask="9999 9999 9999 9999"
            className="billing-cart-number"
            value={userPaymentInfo.cardNumber}
            onChange={(e) => handleChangeInputs('cardNumber', e)}
            placeholder={'Enter card number'}
            labelText={'Card number'}
          />
          <div className="expiry-date-container">
            <label>Expiry Date (MM YYYY)</label>
            <div className="expiry-date-inputs-container">
              <Input
                type="text"
                mask="99"
                value={userPaymentInfo.month}
                onChange={(e) => handleChangeInputs('month', e)}
                placeholder={'MM'}
              />
              <Input
                type="text"
                value={userPaymentInfo.year}
                mask="9999"
                onChange={(e) => handleChangeInputs('year', e)}
                placeholder={'YYYY'}
              />
            </div>
          </div>
          <div className="cvc-container">
            <label className="cvc-label">CVC / CVV
              <Tooltips
                id="cvc-tooltip"
                mask="9999"
                content="3 OR 4 DIGIT NUMBER THAT CAN BE FOUND ON THE BACK OF YOUR CARD NEAR YOUR SIGNATURE."
                placement="top">
                <Icon>help_outline</Icon>
              </Tooltips>
            </label>
            <Input
              type="text" value={userPaymentInfo.cvc}
              onChange={(e) => handleChangeInputs('cvc', e)}
              placeholder={'3 or 4 digits'}
            />
          </div>
        </div>
        <div className="billing-container-column-row row-address-line1">
          <CustomSelect
            options={countries}
            handleChange={(e, v) => {
              setUserPaymentInfo({...userPaymentInfo, country: {value: e, name: v}})
            }}
            value={userPaymentInfo.country.value}
            label='Select Country'
            multiple={false}
          />
          <Input
            type="text"
            value={userPaymentInfo.city}
            onChange={(e) => handleChangeInputs('city', e)}
            placeholder={'Enter city'}
            labelText={'City'}
          />
        </div>
        <div className="billing-container-column-row row-address-line2">
          <Input
            type="text"
            className="billing-address"
            value={userPaymentInfo.address}
            onChange={(e) => handleChangeInputs('address', e)}
            placeholder={'Enter address'}
            labelText={'Address'}
          />
          <Input
            type="text"
            className="billing-state"
            value={userPaymentInfo.state}
            onChange={(e) => handleChangeInputs('state', e)}
            placeholder={'Enter state'}
            labelText={'State'}
          />
          <Input
            type="text"
            mask="99999999999"
            className="billing-zip"
            value={userPaymentInfo.zip}
            onChange={(e) => handleChangeInputs('zip', e)}
            placeholder={'Enter zip code'}
            labelText={'Zip Code'}
          />
        </div>
        <div className="update-payment-method-btn-container">
          <Button className="billing-btn">Upgrade card</Button>
          <Button className="billing-btn-cancel" onClick={() => setChangeCard(!isChangeCard)}>Cancel</Button>
        </div>
      </div> :
      <div className="billing-container-content billing-container-column-content">
        <div className="payment-method-user-name">{userPaymentInfo.name}</div>
        <div className="payment-method-user-card">{userPaymentInfo.cardNumber}</div>
        <Button className="billing-btn payment-method-btn" onClick={() => setChangeCard(!isChangeCard)}>Change
          card</Button>
      </div>}
  </div>;
}

export default React.memo(PaymentMethod);