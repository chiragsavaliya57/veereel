//import you service

import GetVideoInfoService from "./GetVideoInfo"
import AuthService from "./Auth"
import CookieService from "./Cookie"
import UserService from "./User"
import AdCreatorService from "./AdsCreator";
import setNotificationMessageService from "./setNotificationMessage";


export default {
  GetVideoInfoService,
  AuthService,
  CookieService,
  UserService,
  AdCreatorService,
  setNotificationMessageService,
}
