import React from "react";
import "./style.css";
import cx from "classnames";

function ComingSoon() {
  return (
    <div className="coming-soon-container">
      <h1 className="coming-soon-title">Coming Soon</h1>
      <h3 className="coming-soon-subtitle">Our website is under construction!</h3>

    </div>
  );
}

export default React.memo(ComingSoon);
