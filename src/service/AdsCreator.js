import ApiService from './Api'

const toBase64 = file => new Promise((resolve, reject) => {
const reader = new FileReader();
  reader.onload = () => resolve(reader.result);
  reader.onerror = error => reject(error);
  reader.readAsDataURL(file);
	});

export default {
	postVideo(changes) {
		return ApiService.post('/videos/post.php', changes);
	},
	postSession(args) {
		return ApiService.post('/videos/sessions/post.php', args);
	  },
	  getSession() {
		return ApiService.get('/videos/sessions/get.php')
	  },
	getTasks() {
		return ApiService.get('/videos/tasks/get.php')
	},
	getVideo() {
		return ApiService.get('/videos/get.php')
	},
	uploadImage(image, size) {
		const extension = image.name.split('.').pop();
		return toBase64(image).then(blob => ApiService.post('/videos/images/post.php', {blob, extension, size}));
	  },
	  uploadSound(sound) {
		const extension = sound.name.split('.').pop();
		return toBase64(sound).then(blob => ApiService.post('/videos/audio/post.php', {blob, extension}));
	  },
	  getScriptSuggestion() {
		return ApiService.get('/videos/scripts/get.php')
	  },
	  getCategory() {
		return ApiService.get('/videos/scripts/categories/get.php')
	},

	postNotification(changes) {
		return ApiService.post('/videos/notifications/post.php', changes);
	},
	getNotification() {
		return ApiService.get('/videos/notifications/get.php');
	},

	deleteNotification(data) {
		return ApiService.delete('/videos/notifications/delete.php',data);
	}
};
