import React, { useState, useEffect } from 'react';
import Button from '../Button';
import CustomTable from '../CustomTable';

import PaymentMethod from './PaymentMethod';
import Plan from './Plan';
import BillingInfo from './BillingInfo';

import './style.css';

function Billing({className}) {
  useEffect(() => {
    document.querySelector('.tab-profile-containerRef div[class^=\'CardBody-cardBody-\']').classList.add('billing-profile-tab');
  }, []);

  useEffect(() => () => document.querySelector('.tab-profile-containerRef div[class^=\'CardBody-cardBody-\']').classList.remove('billing-profile-tab'), []);

  return <div className="billing-container">
    <Plan/>
    <PaymentMethod/>
    <BillingInfo/>
    <InvoiceHistory/>
  </div>;
}


function InvoiceHistory() {
  const tableData = [
    [
      {content: <div>February 21, 2019</div>},
      {content: <div>$29.00</div>},
      {content: <a>Download PDF</a>},
    ],
    [
      {content: <div>January 21, 2019</div>},
      {content: <div>$29.00</div>},
      {content: <a>Download PDF</a>},
    ],
  ];
  return <div className="billing-container">
    <h1 className="billing-container-title"> Invoice History</h1>
    <div className="billing-container-content">
      <CustomTable
        tableData={tableData}
      />
    </div>
  </div>;

}

export default React.memo(Billing);
