import React, { useState, useEffect } from 'react';
import Icon from '@material-ui/core/Icon';
import useBoolean from '../../utils/useBoolean';

import './style.css';

function SubHeaderEditable({children, changeBlock, handleSave, initialValue}) {
  const {
    value: isOpen,
    setTrue: open,
    setFalse: close,
  } = useBoolean(false);


  const [inputValue, setInputValue] = useState('');

  const handleClickOnSave = () => {
    handleSave(inputValue);
  };

  useEffect(() => {
    setInputValue(initialValue);
    close();
  }, [initialValue]);

  return (
    <div>
      {
        isOpen ?
          <div className="sub-header-editable">
            {changeBlock({value: inputValue, handleChange: (v) => setInputValue(v), autoFocus: true})}
            <Icon onClick={handleClickOnSave}>save</Icon>
          </div>
          :
          <div className="sub-header-editable" onClick={open}>
            {children}
            <Icon>edit</Icon>
          </div>
      }
    </div>
  );
}

export default React.memo(SubHeaderEditable);
