import React, { useState, useEffect, useCallback } from 'react';
import { getUserInfo, changeInfo } from '../../reducer/user/actions';
import { useDispatch, useSelector } from 'react-redux';

import Button from '../../components/Button';
import Alert from '../../components/Alert';

import Input from '../../components/Input';
import UpdateImage from '../../components/UpdateImage';

import './style.css';
import cx from 'classnames';


function UpdateProfile() {
  const dispatch = useDispatch();
  const {payload: payloadInfo} = useSelector(state => state.user.info);
  const {error, isLoading, payload} = useSelector(state => state.user.changeInfo);
  const [alertInfo, setAlertInfo] = useState({
    text: '',
    type: '',
    isOpen: false,
  });

  const [newUserInfo, setNewUserInfo] = useState({
    name: '',
    avatar: '',
    imageUrl: '',
  });

  useEffect(() => {
    if (payloadInfo) {
      setNewUserInfo({
        ...newUserInfo, ...payloadInfo,
        imageUrl: `https://www.anabasis.online/main/avatars/${payloadInfo.avatar_uuid}.${payloadInfo.avatar_extension}`,
      });
    }
  }, [payloadInfo]);

  const handleChangeInput = (type, e) => {
    setNewUserInfo({...newUserInfo, [type]: e});
  };
  const handleUpdateInfo = () => {
    dispatch(changeInfo({
      name: newUserInfo.name,
      avatar: newUserInfo.avatar,
    }))
      .then(()=> setAlertInfo({text: "Data updated successfully", type: "success", isOpen: true}))
      .catch(()=> setAlertInfo({text: "Temporary problems try again later", type: "error", isOpen: true}));
  };

  const onSave = (e) => {
    setNewUserInfo({...newUserInfo, avatar: e});
  };

  return (
    <div className="update-profile-container">
      {
        newUserInfo.name && <div>
          <div>
            <UpdateImage
              defautImage={newUserInfo.imageUrl}
              onSave={onSave}
            />
          </div>
          <Input
            type="text"
            value={newUserInfo.name}
            onChange={(e) => {
              handleChangeInput('name', e);
            }}
            labelText={'Name'}
          />
          <div className="update-profile-btn-container">
            <Button onClick={handleUpdateInfo} disabled={isLoading}>Update</Button>
          </div>
          <div className="alert-update-profile">
            <Alert
              message={alertInfo.text}
              isOpen={alertInfo.isOpen}
              theme={alertInfo.type}
              handlerClose={() => setAlertInfo({text: '', type: '', isOpen: false})}
            />
          </div>
        </div>
      }
    </div>
  );
}


export default UpdateProfile;