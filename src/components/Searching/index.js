import React, { useEffect } from 'react';
import Button from '../Button';
import Loader from '../Loader';
import './style.css';

function Searching({ status, scanned, founded, handleCancel }) {
  useEffect(() => {
	    var result=document.querySelector('.Dashboard-mainPanel-2 ');
		if(result!=null)
			document.querySelector('.Dashboard-mainPanel-2 ').style.overflow = 'hidden';

    return () => {
		var result2=document.querySelector('.Dashboard-mainPanel-2 ');
		if(result2!=null)
			document.querySelector('.Dashboard-mainPanel-2 ').style.overflow = 'auto';
    };
  }, []);

  return <div className="searching">
    <div className="searching-content">
      <div className="searching-header">
        <div className="searching-header-title">
          Searching
        </div>
        <Loader className="searching-header-loader" />
      </div>
      <div className="searching-status">
        <div className="searching-status-key">
          Status:
        </div>
        <div className="searching-status-value">
          {status}
        </div>
      </div>
      <div className="searching-count">
        <div className="searching-count-key">
          Videos Scanned
        </div>
        <div className="searching-count-value">
          {scanned}
        </div>
      </div>
      <div className="searching-count">
        <div className="searching-status-scanned-key">
          Monetized Videos Found
        </div>
        <div className="searching-count-value">
          {founded}
        </div>
      </div>
      <div className="search-buttons">
        <Button className="search-button" onClick={handleCancel}>cancel</Button>
      </div>
    </div>
  </div>;
}

export default Searching;