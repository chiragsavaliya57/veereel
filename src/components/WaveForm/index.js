import React from 'react';
import WaveSurfer from 'wavesurfer.js';
import Loader from "../Loader";
import defaultAudio from "assets/audio/2.mp3";
import Icon from '@material-ui/core/Icon';

export default class WaveForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {ready: true,play:false};
    this.ws = null;
    this.containerRef = React.createRef();
  }

  componentDidMount() {
    // const params = {...this.props.params, container: this.containerRef.current,
    //   waveColor: 'violet',
    //   progressColor: 'purple'};
    const params = {container: "#waveform"+this.props.index, waveColor: 'violet', progressColor: 'purple'};
    this.ws = WaveSurfer.create(params);
    this.ws.on('ready', () => {
      this.setState({ready: true});
      if (typeof this.props.onReady === 'undefined') {
        //this.props.onReady();
        this.ws.pause();
      }
    });
    if(process.env.NODE_ENV == 'development'){
	  // console.log(defaultAudio);
      this.ws.load(defaultAudio);
    }else{
		var URL = this.props.url;

		var URLsplit = URL.split('/');

		var host = URLsplit[0] + "//" + URLsplit[2] + "/";

		var newURL = URL.replace(host, '/');
	  console.log(newURL);
	  
      this.ws.load(newURL);
    }
  }

  handlePlayAudio(){
    console.log(this.ws.isPlaying())
    if(this.ws.isPlaying()){
      this.setState({play: false});
      this.ws.pause();
    }else{
      this.setState({play: true});
      this.ws.playPause();
    }
  }

  render() {
    return (
      <div>
        {!this.state.ready && <Loader/>}
        {this.ws && <Icon hidden={!this.state.ready} onClick={() => this.handlePlayAudio()}>{((this.state.play)?'pause':'play_arrow')}</Icon>}
        <div id={"waveform"+this.props.index} ref={this.containerRef} hidden={!this.state.ready} style={{height: 130, width: 300}}/> 
      </div>
    )
  }
}
