import api from '../service';
import createReducer from '../reducer';
import {createStore, applyMiddleware, compose} from 'redux';
import thunk from 'redux-thunk';
import { get_cookie } from '../classes/cookies';

const preState = {auth: {login: {error: null, isLoading: false, payload: {}}, registration: {}}};

if (get_cookie('isLoggedIn') === '1') {
  preState.auth.login.payload = {
    email: get_cookie("email")
  }
}

export default function (history) {
  const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
  return createStore(
    createReducer(history),
    preState,
    composeEnhancers(applyMiddleware(thunk.withExtraArgument(api))));
}
