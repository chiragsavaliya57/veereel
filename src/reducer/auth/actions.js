import {
  AUTH_START,
  AUTH_SUCCESS,
  AUTH_FAILED,
  LOGOUT_START,
  LOGOUT_SUCCESS,
  LOGOUT_FAILED,
  REGISTRATION_START,
  REGISTRATION_SUCCESS,
  REGISTRATION_FAILED
} from "./constants";

export const auth = (params) => (dispatch, getState, { AuthService, CookieService }) => {
  dispatch({
    type: AUTH_START
  });

  return AuthService.login(params).then(payload => {
    CookieService.succsessLoginCookie(payload.email);

    return dispatch({
      type: AUTH_SUCCESS,
      payload
    })
  }).catch(payload => {
    // CookieService.logoutCookie();

    return dispatch({
      type: AUTH_FAILED,
      payload
    })
  })
};

export const logOut = () => (dispatch, getState, { AuthService, CookieService }) => {
  dispatch({
    type: LOGOUT_START
  });

  return AuthService.logout().then(payload => {
    CookieService.logoutCookie();

    return dispatch({
      type: LOGOUT_SUCCESS,
      payload
    })
  }).catch(payload => {
    return dispatch({
      type: LOGOUT_FAILED,
      payload
    })
  })
};

export const registration = (params) => (dispatch, getState, { AuthService }) => {
  dispatch({
    type: REGISTRATION_START
  });

  return AuthService.registration(params).then(payload => {
    return dispatch({
      type: REGISTRATION_SUCCESS,
      payload
    })
  }).catch(payload => {
    return dispatch({
      type: REGISTRATION_FAILED,
      payload
    })
  })
};
