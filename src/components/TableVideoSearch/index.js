import React, { useState, useEffect, useMemo } from 'react';
import CustomTable from '../CustomTable';
import Button from '../Button';
import { useDispatch, useSelector } from 'react-redux';
import { sortObjects } from '../../utils/sort';
import VideoInfoVideoSearch from '../VideoInfoVideoSearch';
import Icon from '@material-ui/core/Icon';
import PrintDate from '../PrintDate';
import PrintNumber from '../PrintNumber';
import Checkbox from '@material-ui/core/Checkbox';
import Searching from '../../components/Searching';

import './style.css';


function TableVideoSearch() {
  const [selected, setSelected] = useState([]);
  const [sort, setSort] = useState(null);
  const {error, isLoading, payload} = useSelector(state => state.videoInfo);


  const selectItem = (videoid) => {
    if (selected.includes(videoid)) {
      setSelected(selected.filter(i => i !== videoid));
    } else {
      setSelected([...selected, videoid]);
    }
  };

  const selectAll = () => {
    if (selected.length === payload.items.length) {
      setSelected([]);
    } else {
      setSelected(payload.items.map(i => i.videoid));
    }
  };

  const sortinng = field => {
    if (sort && sort.field === field) {
      if (sort.order === 'up') {
        setSort(null);
        return;
      }
      setSort({
        field,
        order: 'up',
      });
    } else {
      setSort({
        field,
        order: 'down',
      });
    }
  };
  const getIconName = field => {
    if (sort && sort.field === field) {
      return `arrow_drop_${sort.order}`;
    }
    return 'sort';
  };

  const sortedData = useMemo(() => {
    return payload && sortObjects(payload.items || [], sort);
  }, [payload, sort]);

  useEffect(() => {
    setSort(null);
  }, [payload]);

  return (
    <div className="table-video-search-container">
      {selected}
      <div className="table-video-search-header-container">
        <h1 className="table-video-search-title">Search Results</h1>
        <div className="table-video-search-btn-container">
          <Button onClick={() => {
          }}>Add to list</Button>
          <Button onClick={() => {
          }}>Get Links</Button>
          <Button onClick={() => {
          }}>Export</Button>
        </div>
      </div>
      <CustomTable className="table-video-search-result-table"
                   tableHead={[
                     {
                       content: <div className="with-sort" onClick={selectAll}>Select</div>,
                       className: 'min',
                     },
                     {
                       content: <div className="with-sort"
                                     onClick={() => sortinng('videoid')}>Video<Icon>{getIconName('videoid')}</Icon>
                       </div>,
                       className: 'max',
                     },
                     {
                       content: <div className="with-sort"
                                     onClick={() => sortinng('viewCount')}>Views<Icon>{getIconName('viewCount')}</Icon>
                       </div>,
                     },
                     {
                       content: <div className="with-sort"
                                     onClick={() => sortinng('likeCount')}>Likes<Icon>{getIconName('likeCount')}</Icon>
                       </div>,
                     },
                     {
                       content: <div className="with-sort"
                                     onClick={() => sortinng('dislikeCount')}>Dislikes<Icon>{getIconName('dislikeCount')}</Icon>
                       </div>,
                     },
                     {
                       content: <div className="with-sort"
                                     onClick={() => sortinng('commentCount')}>Comments<Icon>{getIconName('commentCount')}</Icon>
                       </div>,
                     },
                     {
                       content: <div className="with-sort"
                                     onClick={() => sortinng('publishedat')}>Published<Icon>{getIconName('publishedat')}</Icon>
                       </div>,
                     }]}
                   tableData={sortedData ? sortedData.map(i => ([
                     {
                       content: <Checkbox
                         className="table-video-search-checkbox"
                         checked={selected.includes(i.videoid)}
                         onClick={() => {
                           selectItem(i.videoid);
                         }}/>,
                       className: 'min',
                     },
                     {
                       content: <VideoInfoVideoSearch
                         id={i.videoid}
                         info={i.description}
                       />,
                       className: 'max',
                     },
                     {
                       content: <div><PrintNumber number={i.viewCount}/></div>,
                     },
                     {
                       content: <div><PrintNumber number={i.likeCount}/></div>,
                     },
                     {
                       content: <div><PrintNumber number={i.dislikeCount}/></div>,
                     },
                     {
                       content: <div><PrintNumber number={i.commentCount}/></div>,
                     },
                     {
                       content: <div><PrintDate ts={i.publishedat}/></div>,
                     },
                   ])) : []}
      />
      {isLoading && <Searching handleCancel={() => {
      }}/>}
    </div>
  );
}

export default TableVideoSearch;