import React from 'react';
import axios from 'axios';

class CheckLogin extends React.Component {
	/*constructor(props) {
		super(props);
		this.Perform = this.Perform.bind(this);
	}*/
	static Perform(callback) {
	  axios
		.get(document.location.origin+"/api/getLogin.php")
		.then(({ data }) => callback );
	}
}

export default CheckLogin;