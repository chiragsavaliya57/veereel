import qs from 'qs';

const CONTENT_TYPE_JSON = 'application/json';

export function prepareQueryString(params) {
  return qs.stringify(params, { encode: false, arrayFormat: 'brackets' });
}

export function parseQueryString(queryString) {
  return qs.parse(queryString, { ignoreQueryPrefix: true });
}

class ApiService {


  constructor() {
    this.apiPrefix = '/api';
    this.logoutCallbacks = [];
  }

  getApiLink(link, params) {
    return this.apiPrefix + link + (params ? '?' + prepareQueryString(params) : '');
  }

  call(url, method = 'GET', options = {}, params = null) {
    const headers = options.headers || {};

    for (let headerKey in (options.headers || {})) {
      if (options.headers.hasOwnProperty(headerKey)) {
        headers[headerKey] = options.headers[headerKey];
      }
    }

    options.headers = headers;
    options.method = method;
    options.credentials = 'include';
    options.mode = 'cors';

    return fetch(this.getApiLink(url, params), options)
      .then(resp => {
        let result;
        result = resp.text();

        return Promise.all([result, resp.status]);
      })
      .then(([data, status]) => {
        if (status === 403) {
          this.callLogoutCallbacks();
        }

        if (status >= 500 || [400, 401, 402, 403, 404].includes(status)) {
          return Promise.reject(data);
        }

        try {
          data = JSON.parse(data);
          if (data.responseResult && data.responseResult.items) {
            const newItems = [];

            data.responseResult.items.forEach((item) => {
              newItems.push(Object.assign({}, ...Object.keys(item).map((k) => ({[k]: /^\+?[1-9][\d]*$/.test(item[k]) ? parseInt(item[k]) : item[k]}))));
            });

            data.responseResult.items = newItems;
          }
        } catch (e) {
          if (data.trim() === "") {
            data = {
              responseFlag: true,
              responseResult: null
            }
          }
        }

        if (data.responseFlag) {
          return (typeof data.responseResult === 'undefined') ? data : data.responseResult;
        } else {
          return Promise.reject(data.responseError || data.errors || data.error || data);
        }
      });
  }

  get(url, params = null, options = {}) {
    return this.call(url, 'GET', options, params);
  }

  post(url, data = null, options = {}) {
    if (data) {
      options.body = JSON.stringify(data);
      options.headers = {
        'Content-Type': CONTENT_TYPE_JSON
      };
    }

    return this.call(url, 'POST', options);
  }

  put(url, data = null, options = {}) {
    if (data) {
      options.body = JSON.stringify(data);
      options.headers = {
        'Content-Type': CONTENT_TYPE_JSON
      };
    }

    return this.call(url, 'PUT', options);
  }

  upload(url, file, name) {
    const formData = new FormData();
    formData.append(name, file);

    const options = {
      body: formData
    };

    return this.call(url, 'POST', options);
  }

  delete(url, data = null, options = {}) {
    if(data){
      options.body = JSON.stringify(data);
      options.headers = {
        'Content-Type': CONTENT_TYPE_JSON
      };
    }
    return this.call(url, 'DELETE', options);
  }

  callLogoutCallbacks() {
    this.logoutCallbacks(callback => callback && callback());
  }

  addLogoutCallback(callback) {
    this.logoutCallbacks.push(callback);
  }
}

export default new ApiService();