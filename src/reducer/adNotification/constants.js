export const SET_NOTIFICATION_MESSAGES = 'set_notification_message/start';
export const SET_NOTIFICATION_MESSAGES_SUCCESS = 'set_notification_message/success';
export const SET_NOTIFICATION_MESSAGES_FAILED = 'set_notification_message/failed';