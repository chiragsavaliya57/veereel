import React, { useState, useEffect } from 'react';
import Button from '../Button';


function Plan() {
  const [currentPlan, setCurrentPlan] = useState('free');
  return <div className="billing-container">
    <h1 className="billing-container-title">Your plan</h1>
    <div className="billing-container-content plan-container-content">
      <div className="current-plan">{currentPlan}</div>
      <Button className="billing-btn">Upgrade plan</Button>
    </div>
  </div>;
}

export default React.memo(Plan);