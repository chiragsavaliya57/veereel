import axios from 'axios';

const transport = axios.create({
  baseURL: 'http://localhost:3000/api/',
});

export default transport;
