import React, { useState, useRef, useEffect } from 'react';

import cx from 'classnames';
import './style.css';

function PopupMainPage({children, type, changeIsOpen, isOpen}) {
  const node = useRef();

  useEffect(() => {
    document.addEventListener('mousedown', handleClick);
    return () => {
      document.removeEventListener('mousedown', handleClick);
    };
  }, []);

  const handleClick = e => {
    if (node.current.contains(e.target)) {
      return;
    }
    changeIsOpen(type, false);
  };


  return (
    <div className={cx('popup-main-page', isOpen ? 'db' : 'dn')} ref={node}>
      <div className={`popup-main-page-arrow-${type}`}></div>
      {children}
    </div>
  );
}

export default React.memo(PopupMainPage);
