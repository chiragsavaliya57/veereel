import {
    SET_NOTIFICATION_MESSAGES,
} from "./constants";

const initialState = {
    messages: []
};

export default function (state = initialState, action) {
    switch (action.type) {
        case SET_NOTIFICATION_MESSAGES: {
            return { ...state, messages: action.messages };
        }
        default:
            return state
    }
}