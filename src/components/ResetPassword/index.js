import React, { useState } from 'react';
import Button from '../../components/Button';
import Input from '../../components/Input';

import './style.css';

function ResetPassword() {
  const [isLoading, setIsLoading] = useState(false);
  const [textReset, setTextReset] = useState('We’ll send instructions to your email to reset your password.');
  const [userEmail, setUserEmail] = useState(null);
  const [isValid, setIsValid] = useState({
    email: true,
  });

  const handleSubmit = () => {
    const checkValid = {email: /^\w+\@\w+\.\w{2,4}$/.test(userEmail)};
    setIsValid(checkValid);

    if (checkValid.email) {
      setTextReset(`Instructions for reset password are send to ${userEmail}`);
      setIsLoading(true)
    }
  };

  return (
    <div className="reset-password-main-page">
      <h1 className="reset-password-main-page-header">Password reset</h1>
      <div className="reset-password-main-page-content">
        <p className="reset-password-text-reset">{textReset}</p>
        <Input
          type="text"
          value={userEmail}
          onChange={(e) => setUserEmail(e)}
          className="white-input"
          placeholder="Enter your email"
          disabled={isLoading}
          error={!isValid.email}
          helperText={!isValid.email && "Email is not valid"}
        />
      </div>
      <div className="reset-password-main-page-btn-container">
        <Button onClick={handleSubmit}  disabled={isLoading}>RESET</Button>
      </div>
    </div>
  );
}

export default React.memo(ResetPassword);
