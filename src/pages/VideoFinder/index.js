import React, { useState } from 'react';
// import { useSelector } from 'react-redux';

import VideoSearch from '../../components/VideoSearch';
import Tabs from '../../components/Tabs';
import ComingSoon from '../../components/ComingSoon'
import TableVideoSearch from '../../components/TableVideoSearch'

import './style.css';

function VideoFinder() {
  const tabData = [
    {
      tabName: <div className="tab-video-finder-title">Video search</div>,
      tabContent: <VideoSearch />
    },
    {
      tabName: <div className="tab-video-finder-title">Chanel search</div>,
      tabContent: <ComingSoon />
    },
    {
      tabName: <div className="tab-video-finder-title">Keyword ideas</div>,
      tabContent:  <ComingSoon />
    },
    {
      tabName: <div className="tab-video-finder-title">Placement list</div>,
      tabContent:  <ComingSoon />
    },
    {
      tabName: <div className="tab-video-finder-title">Video info</div>,
      tabContent:  <ComingSoon />
    }
  ];
  return (
    <div className="tab-video-finder">
      <Tabs tabData={tabData}/>
      <TableVideoSearch />
    </div>
  );
}

export default VideoFinder;