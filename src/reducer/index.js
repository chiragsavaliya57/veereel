import { combineReducers } from 'redux';
import videoInfo from './videoInfo';
import auth from './auth';
import user from './user';
import adCreator from './adCreator';
import adNotification from './adNotification';

import { connectRouter } from "connected-react-router";

export default history => combineReducers({
  router: connectRouter(history),
  videoInfo,
  auth,
  user,
  adCreator,
  adNotification,
});
