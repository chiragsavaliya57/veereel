import {
    SET_NOTIFICATION_MESSAGES,
    // SET_NOTIFICATION_MESSAGES_SUCCESS,
    // SET_NOTIFICATION_MESSAGES_FAILED,
} from './constants';

export const setNotificationMessage = (message) => (dispatch, getState, { setNotificationMessageService }) => {
    return dispatch({
        type: SET_NOTIFICATION_MESSAGES, 
        message
    });

    // return setNotificationMessageService.setMessages(message).then(payload => {
    //     dispatch({
    //         type: SET_NOTIFICATION_MESSAGES_SUCCESS,
    //         payload
    //     })
    // }).catch(payload => {
    //     dispatch({
    //         type: SET_NOTIFICATION_MESSAGES_FAILED,
    //         payload
    //     })
    // })
};