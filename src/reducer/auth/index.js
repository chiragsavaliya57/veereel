import {
  AUTH_START,
  AUTH_SUCCESS,
  AUTH_FAILED,
  LOGOUT_START,
  LOGOUT_SUCCESS,
  LOGOUT_FAILED,
  REGISTRATION_START,
  REGISTRATION_SUCCESS,
  REGISTRATION_FAILED
} from "./constants";

const initialState = {
 login: {
   error: null,
   isLoading: false,
   payload: null,
 },
  registration: {
    error: null,
    isLoading: false,
    payload: null,
  }
};

export default function (state = initialState, action) {
  switch (action.type) {
    case AUTH_START: {
      return {...state, login:{ error: null, isLoading: true}};
    }
    case AUTH_SUCCESS: {
      return {...state, login: { error: null, isLoading: false, payload: action.payload}};
    }
    case AUTH_FAILED: {
      return {...state, login: { payload: null, isLoading: false, error: action.payload}};
    }
    case LOGOUT_START: {
      return {...state, login:{ error: null, isLoading: true}};
    }
    case LOGOUT_SUCCESS: {
      return {...state, login: { error: null, isLoading: false, payload: null}};
    }
    case LOGOUT_FAILED: {
      return {...state, login: { payload: null, isLoading: false, error: action.payload}};
    }
    case REGISTRATION_START: {
      return {...state, registration: { payload: null, isLoading: true, error: null}};
    }
    case REGISTRATION_SUCCESS: {
      return {...state, registration: { payload: true, isLoading: false, error: null}};
    }
    case REGISTRATION_FAILED: {
      return {...state, registration: { payload: false, isLoading: false, error: action.payload}};
    }
    default:
      return state
  }
}
