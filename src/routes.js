import DashboardIcon from '@material-ui/icons/Dashboard';
import Image from '@material-ui/icons/Image';
import {
  TrendingUp,
  Assessment,
  Palette,
  OndemandVideo,
  Settings,
  ImageSearch,
  Timeline
} from '@material-ui/icons';

import VideoFinder from './pages/VideoFinder';
import MyVideos from './pages/MyVideos/MyVideos';
import ROICalculator from './pages/ROICalculator/ROICalculator.js';
import CommingSoon from './components/ComingSoon'
import VideoSearch from './assets/img/image_search.svg'
import AdCreatorEditor from './pages/AdCreator/Editor';
import AdCreator from './pages/AdCreator/AdCreator';

var dashRoutes = [
  {
    path: '/video-finder',
    name: 'Video finder',
    rtlName: 'هعذاتسجيل الدخول',
    icon: VideoSearch,
    rtlMini: 'VideoFinder',
    component: VideoFinder,
    layout: '/main',
  },
  {
    path: '/reporting',
    name: 'Reporting',
    rtlName: 'لوحة القيادة',
    icon: Timeline,
    component: CommingSoon,
    layout: '/main',
  },
  {
    path: '/roicalculator',
    name: 'ROI Calculator',
    rtlName: 'لوحة القيادة',
    icon: Assessment,
    component: ROICalculator,
    layout: '/main',
  },
  {
    path: "/ad-creator",
    name: "Ad Creator",
    rtlName: "لوحة القيادة",
    icon: Palette,
    component: AdCreator,
    layout: "/main",
	  exact: true
  },
  {
    path: "/ad-creator/editor",
    name: "Ad Creator 2",
    rtlName: "لوحة القيادة",
    icon: Palette,
    component: AdCreatorEditor,
    layout: "/main",
	  redirect:true
  },
  {
    path: '/my-videos',
    name: 'My Videos',
    rtlName: 'لوحة القيادة',
    icon: OndemandVideo,
    component: MyVideos,
    layout: '/main',
  },
  {
    path: '/settings',
    name: 'Settings',
    rtlName: 'لوحة القيادة',
    icon: Settings,
    component: CommingSoon,
    layout: '/main',
  }
];
export default dashRoutes;
