import {
  GET_USER_START,
  GET_USER_SUCCESS,
  GET_USER_FAILED,
  CHANGE_PASSWORD_START,
  CHANGE_PASSWORD_SUCCESS,
  CHANGE_PASSWORD_FAILED,
  CHANGE_INFO_START,
  CHANGE_INFO_SUCCESS,
  CHANGE_INFO_FAILED,
} from "./constants";

export const getUserInfo = () => (dispatch, getState, { UserService, CookieService }) => {
  dispatch({
    type: GET_USER_START
  });
  return UserService.getInfo().then(payload => {
    return dispatch({
      type: GET_USER_SUCCESS,
      payload
    })
  }).catch(payload => {
    CookieService.logoutCookie();

    return dispatch({
      type: GET_USER_FAILED,
      payload
    })
  })
};


export const changePassword = (params) => (dispatch, getState, { UserService }) => {
  dispatch({
    type: CHANGE_PASSWORD_START
  });
  return UserService.changePassword(params).then(() => {

    return dispatch({
      type: CHANGE_PASSWORD_SUCCESS,
    })
  }).catch(payload => {
    return dispatch({
      type: CHANGE_PASSWORD_FAILED,
      payload
    })
  })
};

export const changeInfo = (params) => (dispatch, getState, { UserService }) => {
  dispatch({
    type: CHANGE_INFO_START
  });
  return UserService.changeInfo(params).then(() => {

    return dispatch({
      type: CHANGE_INFO_SUCCESS,
      payload: params
    })
  }).catch(payload => {
    return dispatch({
      type: CHANGE_INFO_FAILED,
      payload
    })
  })
};