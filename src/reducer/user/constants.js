export const GET_USER_START   = 'get_user/start';
export const GET_USER_SUCCESS = 'get_user/success';
export const GET_USER_FAILED  = 'get_user/failed';

export const CHANGE_PASSWORD_START   = 'change_password/start';
export const CHANGE_PASSWORD_SUCCESS = 'change_password/success';
export const CHANGE_PASSWORD_FAILED  = 'change_password/failed';


export const CHANGE_INFO_START   = 'change_info/start';
export const CHANGE_INFO_SUCCESS = 'change_info/success';
export const CHANGE_INFO_FAILED  = 'change_info/failed';