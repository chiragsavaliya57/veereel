import React from 'react';
import axios from 'axios';
import './ROICalculator.css'

import PropTypes from "prop-types";
// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";
import FormLabel from "@material-ui/core/FormLabel";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import InputAdornment from "@material-ui/core/InputAdornment";
import Radio from "@material-ui/core/Radio";
import Checkbox from "@material-ui/core/Checkbox";
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';

// core components
import GridContainer from "components/Grid/GridContainer.jsx";
import GridItem from "components/Grid/GridItem.jsx";
import CustomInput from "components/CustomInput/CustomInput.jsx";
import Button from "components/CustomButtons/Button.jsx";
// import Table from "components/Table/Table.jsx";
import Card from "components/Card/Card.jsx";
import CardHeader from "components/Card/CardHeader.jsx";
import CardText from "components/Card/CardText.jsx";
import CardIcon from "components/Card/CardIcon.jsx";
import CardBody from "components/Card/CardBody.jsx";

import regularFormsStyle from "assets/jss/material-dashboard-pro-react/views/regularFormsStyle";

class ROICalculator extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    const {classes} = this.props;
    const data = [
      {
        name: "Views",
        value: "80,000"
      },
      {
        name: "Clicks on site",
        value: "3,200"
      },
      {
        name: "Cost per click",
        value: "$1,25"
      },
      {
        name: "Leads X Convertion",
        value: "1,632"
      },
      {
        name: "Cost per lead",
        value: "$2.45"
      },
      {
        name: "# Customers",
        value: "49"
      },
      {
        name: "Cost per customer",
        value: "$82"
      },
      {
        name: "Total customer value",
        value: "$6,218"
      },
      {
        name: "Estimated ROI",
        value: "155.35%"
      }
    ];

    return (
      <React.Fragment>
        <Card className="custom-card roi-calc">
          {/* <Card xs={6} sm={4} md={4}> */}
          <CardHeader color="rose" icon>
            <h3 className='sub-title'>Configuration</h3>
          </CardHeader>
          <CardBody>
            <GridContainer>
              <GridItem xs={12} sm={12} md={6} lg={4}>
                <form>
                  <div className="input-text">
                    <label>Ad Spend</label>
                    <CustomInput
                      // labelText="Email adress"
                      id="email_adress"
                      formControlProps={{
                        fullWidth: true
                      }}
                      inputProps={{
                        type: "email",
                        startAdornment: (
                          <InputAdornment position="start" className="roi-input-prefix">$</InputAdornment>
                        )
                      }}
                    />
                    <span>Money spend buying Ads</span>
                  </div>
                  <label>Lead Convertion Rate</label>
                  <CustomInput
                    id="text1"
                    formControlProps={{
                      fullWidth: true
                    }}
                    inputProps={{
                      type: "text",
                      autoComplete: "off"
                    }}
                  />
                  <span>% of clicks turn into leads</span>
                </form>
              </GridItem>
              <GridItem xs={12} sm={12} md={6} lg={4}>
                <form>
                  <div className="input-text">
                    <label>Average Cost Per View</label>
                    <CustomInput
                      // labelText="Email adress"
                      id="email_adress"
                      formControlProps={{
                        fullWidth: true
                      }}
                      inputProps={{
                        type: "email",
                        startAdornment: (
                          <InputAdornment position="start" className="roi-input-prefix">$</InputAdornment>
                        )
                      }}
                    />
                    <span>   .</span>
                  </div>
                  <label>Customer Conversion Rate</label>
                  <CustomInput
                    id="text1"
                    formControlProps={{
                      fullWidth: true
                    }}
                    inputProps={{
                      type: "text",
                      autoComplete: "off"
                    }}
                  />
                  <span>% of clicks turn into leads</span>
                </form>
              </GridItem>
              <GridItem xs={12} sm={12} md={6} lg={4}>
                <form>
                  <div className="input-text">
                    <label>Average Click Thru Rate</label>
                    <CustomInput
                      // labelText="Email adress"
                      id="email_adress"
                      formControlProps={{
                        fullWidth: true
                      }}
                      inputProps={{
                        type: "email"
                      }}
                    />
                    <span>% of views into clicks</span>
                  </div>
                  <label>Customer Value</label>
                  <CustomInput
                    id="text3"
                    formControlProps={{
                      fullWidth: true
                    }}
                    inputProps={{
                      type: "text",
                      autoComplete: "off",
                      startAdornment: (
                        <InputAdornment position="start" className="roi-input-prefix">$</InputAdornment>
                      )
                    }}
                  />
                  <span>Average customer value</span>
                </form>
              </GridItem>
            </GridContainer>
          </CardBody>
        </Card>
        <Card className="custom-card roi-calc-bottom">
          <CardHeader>
            <div className="title-box">
              <h3 className='sub-title'>Results</h3>
              <div className="right-cover">
                <p>Return on Ad Spend</p>
                <span>$2,218</span>
              </div>
            </div>
          </CardHeader>
          <CardBody>
            <GridContainer>
              <GridItem xs={12} sm={12} md={12}>
                <Table className={classes.table}>
                  <TableBody>
                    {data.map(row => (
                      <TableRow key={row.name}>
                        <TableCell component="th" scope="row">
                          {row.name}
                        </TableCell>
                        <TableCell align="right">{row.value}</TableCell>
                      </TableRow>
                    ))}
                  </TableBody>
                </Table>
                            </GridItem>
                        </GridContainer>
                    </CardBody>
                </Card>
            </React.Fragment>
        );
    }
}

ROICalculator.propTypes = {
    classes: PropTypes.object
};


export default withStyles(regularFormsStyle)(ROICalculator);