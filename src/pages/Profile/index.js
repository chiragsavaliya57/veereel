import React, { useState, useEffect } from 'react';
import Tabs from '../../components/Tabs';
import UpdateProfile from '../../components/UpdateProfile';
import Billing from '../../components/Billing';
import UpdatePassword from '../../components/UpdatePassword';
import { NavLink } from 'react-router-dom';

import './style.css';

function Profile() {
  const [active, setActive] = useState(['#profile', '#billing', '#password'].indexOf(window.location.hash));

  const tabData = [
    {
      tabName: <NavLink className="tab-profile-title" to="#profile">Profile</NavLink>,
      isActive: true,
      tabContent: <UpdateProfile/>,
    },
    {
      tabName: <NavLink className="tab-profile-title" to="#billing">Billing</NavLink>,
      tabContent: <Billing/>,
    },
    {
      tabName: <NavLink className="tab-profile-title" to="#password">Password</NavLink>,
      tabContent: <UpdatePassword/>,
    },
  ];

  return (
    <div className="tab-profile-container">
      <Tabs tabData={tabData}
            isActive={active === -1 ? 0 :  active}/>
    </div>
  );
}

export default Profile;