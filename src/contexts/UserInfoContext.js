
import React from 'react';

export const UserInfoContext = React.createContext({
  isLoggedIn: false,
  email: 'Not logged in',
  login: () => {},
  logout: () => {},
});