import React, { useState } from 'react';
import Button from '../Button';
import Hero from "../../assets/img/hero-vidnetcis@2x.png"

import './style.css';

function WhyVidnetics() {

  return (
    <div className="main-page-container-why-vidnetics">
      <div className="main-page-why-vidnetics-left-content">
        <div className="main-page-why-vidnetics-subtitle"><span>No</span> more hassle when trying to</div>
        <h1 className="main-page-why-vidnetics-header">Find Monetized Videos</h1>
        <div className="main-page-why-vidnetics-desc">Find the right monetized videos for your campaign is so time
          consuming right? Wrong! Vidnetics provide you with the exact data you need in real time to make to most of
          your campaigns.
        </div>
        <Button className="main-page-why-vidnetics-demo-btn">TRY DEMO</Button>
      </div>
      <div className="hero-image-container">
        {/*<div className="hero-image-shadow"></div>*/}
        <div className="hero-image-content" style={{backgroundImage: `url(${Hero})`}}></div>
      </div>
    </div>
  );
}

export default WhyVidnetics;