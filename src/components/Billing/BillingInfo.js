import React, { useState, useEffect } from 'react';
import Button from '../Button';
import Input from '../Input';
import CustomSelect from '../CustomSelect';


function BillingInfo() {
  const [userBillingInfo, setUserBillingInfo] = useState({
    address: 'Boulevard St. 342, Pandelis',
    zip: '87432642391',
    state: null,
    country: {value: null, name: null},
    city: null,
    company: null
  });
  const [isChangeBillingInfo, setIsChangeBillingInfo] = useState(false);
  const countries = ['Russia', 'USA', 'UK'];

  const handleChangeInputs = (type, e) => {
    setUserBillingInfo({...userBillingInfo, [type]: e});
  };


  return <div className="billing-container">
    <h1 className="billing-container-title">Billing Info</h1>
    {isChangeBillingInfo ?
      <div className="billing-container-content billing-container-column-content billing-info-container-content">
        <div className="billing-container-column-row billing-info-address-line1">
          <Input
            type="text"
            value={userBillingInfo.address}
            onChange={(e) => handleChangeInputs('address', e)}
            placeholder={'Enter billinsg address'}
            labelText={'Billinsg Address'}
          />
          <Input
            type="text"
            value={userBillingInfo.state}
            onChange={(e) => handleChangeInputs('state', e)}
            placeholder={'Enter state'}
            labelText={'State'}
          />
          <Input
            type="text"
            value={userBillingInfo.zip}
            onChange={(e) => handleChangeInputs('zip', e)}
            placeholder={'Enter zip code'}
            labelText={'Zip Code'}
            mask="99999999999"
          />
        </div>
        <div className="billing-container-column-row billing-info-address-line2">
          <CustomSelect
            options={countries}
            handleChange={(e, v) => {
              setUserBillingInfo({...userBillingInfo, country: {value: e, name: v}});
            }}
            value={userBillingInfo.country.value}
            label='Select Country'
            multiple={false}
          />
          <Input
            type="text"
            value={userBillingInfo.city}
            onChange={(e) => handleChangeInputs('city', e)}
            placeholder={'Enter city'}
            labelText={'City'}
          />
        </div>
        <div className="billing-container-column-row billing-info-address-company-name">
          <Input
            type="text"
            value={userBillingInfo.company}
            onChange={(e) => handleChangeInputs('company', e)}
            placeholder={'Enter company'}
            labelText={'Company'}
            className="billing-info-address-company-name-input"
          />
        </div>
        <div className="billing-info-btn-container">
          <Button className="billing-btn">Save</Button>
          <Button className="billing-btn-cancel" onClick={() => setIsChangeBillingInfo(!isChangeBillingInfo)}>Cancel</Button>
        </div>
      </div> :
      <div className="billing-container-content billing-container-column-content">
        <div className="payment-method-user-address">{userBillingInfo.address}</div>
        <div className="payment-method-user-tel">{userBillingInfo.tel}</div>
        <Button className="billing-btn payment-method-btn"
                onClick={() => setIsChangeBillingInfo(!isChangeBillingInfo)}>Update</Button>
      </div>}

  </div>;
}

export default React.memo(BillingInfo);