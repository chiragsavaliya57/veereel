import ApiService from './Api';

export default {
  login(params) {
    return ApiService.post('/account/login/post.php', params);
  },
  logout() {
    return ApiService.get('/session_ender.php');
  },
  registration(params) {
    return ApiService.post('/account/register/post.php', params);
  },
};