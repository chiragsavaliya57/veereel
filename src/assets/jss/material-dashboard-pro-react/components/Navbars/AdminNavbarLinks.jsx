/*!
=========================================================
* Material Dashboard PRO React - v1.7.0
=========================================================
* Product Page: https://www.creative-tim.com/product/material-dashboard-pro-react
* Copyright 2019 Creative Tim (https://www.creative-tim.com)
* Coded by Creative Tim
=========================================================
* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
*/
import React from "react";
import classNames from "classnames";
import PropTypes from "prop-types";
// import { Manager, Target, Popper } from "react-popper";

// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";
import MenuItem from "@material-ui/core/MenuItem";
import MenuList from "@material-ui/core/MenuList";
import ClickAwayListener from "@material-ui/core/ClickAwayListener";
import Paper from "@material-ui/core/Paper";
import Grow from "@material-ui/core/Grow";
import Hidden from "@material-ui/core/Hidden";
import Popper from "@material-ui/core/Popper";
import Divider from "@material-ui/core/Divider";


// @material-ui/icons
import Person from "@material-ui/icons/Person";
import Notifications from "@material-ui/icons/Notifications";
import Dashboard from "@material-ui/icons/Dashboard";
import Search from "@material-ui/icons/Search";

// creates a beautiful scrollbar
import PerfectScrollbar from "perfect-scrollbar";
import "perfect-scrollbar/css/perfect-scrollbar.css";

// core components
import CustomInput from "components/CustomInput/CustomInput.jsx";
import Button from "components/CustomButtons/Button.jsx";
import Icon from '@material-ui/core/Icon';
import "./style.css";

import adminNavbarLinksStyle from "assets/jss/material-dashboard-pro-react/components/adminNavbarLinksStyle.jsx";

import { getNotification, deleteNotification } from '../../../../../reducer/adCreator/actions';
import AdsCreatorService from "../../../../../service/AdsCreator";
import { connect } from 'react-redux';

class HeaderLinks extends React.Component {

  constructor(props) {
    super(props);
  }

  state = {
    openNotification: false,
    openProfile: false,
    color: '',
    notificationList: []
  };

  handleClickNotification = () => {
    this.setState({ openNotification: !this.state.openNotification });
  };
  handleCloseNotification = () => {
    this.setState({ openNotification: false });
  };
  handleClickProfile = () => {
    this.setState({ openProfile: !this.state.openProfile });
  };
  handleCloseProfile = () => {
    this.setState({ openProfile: false });
  };

  handleClick = () => {
    this.setState({ openNotification: !this.state.openNotification });
    // console.log(this.state.openNotification)
    
    if(this.state.openNotification === false){
      setTimeout(() => {
        const container = document.querySelector('.notification-box');      
        const ps = new PerfectScrollbar(container, {
          suppressScrollX: true,
          suppressScrollY: false,
        });
      }, 0);
      
    }
  };

  deleteNotification = (id) => {
    const data = {
      "id": id
    }
    // this.props.deleteNotification(data);
    // this.props.getNotification();
    AdsCreatorService.deleteNotification(data).then(res => {
      if (res) {
        this.componentDidMount();
      }
    }).finally(() => { });
  };

  componentDidMount() {
    // this.props.getNotification();
    AdsCreatorService.getNotification().then(res => {
      if (res) {
        this.setState({notificationList: res});
      }
    }).finally(() => { });
  }

  componentWillMount() {
    setInterval( () => {
      // this.props.getNotification();
      this.componentDidMount();
    }, 10000);
  }

  render() {
    const { classes, rtlActive } = this.props;
    const { openNotification, openProfile, notificationList } = this.state;

    const searchButton =
      classes.top +
      " " +
      classes.searchButton +
      " " +
      classNames({
        [classes.searchRTL]: rtlActive
      });
    const dropdownItem = classNames(
      "parellel",
      { [classes.dropdownItemRTL]: rtlActive }
    );
    const wrapper = classNames({
      [classes.wrapperRTL]: rtlActive
    });
    const managerClasses = classNames({
      [classes.managerClasses]: true
    });

    // const data = [
    //   {
    //     rtlActive: false,
    //     rtl: "إجلاء أوزار الأسيوي حين بل, كما",
    //     ltr: "Mike John responded to your email"
    //   },
    //   {
    //     rtlActive: false,
    //     rtl: "شعار إعلان الأرضية قد ذلك",
    //     ltr: "You have 5 new tasks"
    //   },
    //   {
    //     rtlActive: false,
    //     rtl: "ثمّة الخاصّة و على. مع جيما",
    //     ltr: "You're now friend with Andrew"
    //   },
    //   {
    //     rtlActive: false,
    //     rtl: "قد علاقة",
    //     ltr: "Another Notification"
    //   },
    //   {
    //     rtlActive: false,
    //     rtl: "قد فاتّبع",
    //     ltr: "Another One"
    //   }
    // ]

    return (
      <div className={wrapper}>

        <div className={managerClasses}>
          <Button
            color="transparent"
            justIcon
            aria-label="Notifications"
            aria-owns={openNotification ? "notification-menu-list" : null}
            aria-haspopup="true"
            onClick={this.handleClick.bind(this)}
            id={"notifications-header"}
            className={rtlActive ? classes.buttonLinkRTL : classes.buttonLink}
            muiClasses={{
              label: rtlActive ? classes.labelRTL : ""
            }}
            buttonRef={node => {
              this.anchorNotification = node;
            }}
          >
            <Notifications
              className={
                (rtlActive
                  ? classes.links + " " + classes.linksRTL
                  : classes.links)
              }
            />
            <span className={classes.notifications} id={notificationList && notificationList.length != 0 ? "icon-notifications-header-active" : "icon-notifications-header"}></span>
            <Hidden mdUp implementation="css">
              <span
                onClick={() => { }}
                className={classes.linkText}
              >
                {rtlActive ? "إعلام" : "Notification"}
              </span>
            </Hidden>
          </Button>
          <Popper
            open={openNotification}
            anchorEl={this.anchorNotification}
            transition
            disablePortal
            placement="bottom"
            className={classNames({
              [classes.popperClose]: !openNotification,
              [classes.popperResponsive]: true,
              [classes.popperNav]: true
            })}
          >
            {({ TransitionProps }) => (
              <Grow
                {...TransitionProps}
                id="notification-menu-list"
                style={{ transformOrigin: "0 0 0" }}
              >
                <Paper className={classes.dropdown}>
                  <ClickAwayListener onClickAway={this.handleCloseNotification}>
                    <MenuList role="menu" className="notification-box">
                      {
                        // data.map((item, index) => (
                        //   <MenuItem
                        //     key={index}
                        //     onClick={this.handleCloseNotification}
                        //     className={dropdownItem}
                        //   >
                        //     {item.rtlActive
                        //       ? item.rtl
                        //       : item.ltr}
                        //     <div className="icon" onClick={this.deleteNotification.bind(this)}>
                        //       <Icon>close</Icon>
                        //     </div>
                        //   </MenuItem>
                        // ))

                        notificationList.length != 0 ? (
                          notificationList.map((item, index) => (
                            <MenuItem
                              key={index}
                              // onClick={this.handleCloseNotification}
                              className={dropdownItem + (item.type == 0 ? " " + classes.infoBackground : "") + (item.type == 1 ? " " + classes.successBackground : "") + (item.type == 2 ? " " + classes.warningBackground : "")}
                            >
                              {item.message}
                              <div className="icon" onClick={() => this.deleteNotification(item.id)}>
                                <Icon>close</Icon>
                              </div>
                            </MenuItem>
                          ))
                        ) : (
                            <MenuItem
                              className={dropdownItem}
                            >
                              {"No Notification"}
                            </MenuItem>
                          )

                      }
                    </MenuList>
                  </ClickAwayListener>
                </Paper>
              </Grow>
            )}
          </Popper>
        </div>


      </div>
    );
  }
}

HeaderLinks.propTypes = {
  classes: PropTypes.object.isRequired,
  rtlActive: PropTypes.bool
};

const mapStateToProps = ({ router: { location: { state: routerState } }, adCreator: { getNotificationResult }, adCreator: { deleteNotificationResult } }) => ({ routerState, getNotificationResult, deleteNotificationResult });

const mapDispatchToProps = dispatch => ({
  getNotification: () => dispatch(getNotification()),
  deleteNotification: data => dispatch(deleteNotification(data))
});

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(adminNavbarLinksStyle)(HeaderLinks));