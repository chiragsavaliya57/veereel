import React from "react";
import CustomInput from '../../assets/jss/material-dashboard-pro-react/components/CustomInput/CustomInput.jsx';
import cx from "classnames";

import "./style.css";

function Input({value, onChange, disabled, placeholder, className, labelText, helperText, type, autoFocus, error, mask}) {

  return (
    <div className={cx('input-container', className)}>
      <CustomInput
        labelText={labelText}
        inputProps={{
          placeholder: placeholder,
          type: type,
          onChange: (e) => onChange(e.target.value),
          value: value,
          autoFocus:autoFocus,
        }}
        error={error}
        mask={mask}
        className={'input-container'}
        disabled= {disabled}
        helperText={helperText}
      />
    </div>
  );
}

export default  React.memo(Input);
