import React, { useState } from 'react';
import HeaderMainPage from "../../components/HeaderMainPage"
import WhyVidnetics from "../../components/WhyVidnetics"
import Pricing from "../../components/Pricing"
import ComingSoon from "../../components/ComingSoon"
import { BrowserRouter as Router, Route, Link, Redirect, Switch } from "react-router-dom";

import './style.css';

function MainPage() {

  return (
    <Switch>
      <div className="main-page-container">
        <HeaderMainPage/>
        <Redirect from="/" to="/why-vidnetics"/>
        <Route path="/why-vidnetics" component={WhyVidnetics}/>
        <Route path="/pricing" component={Pricing}/>
        <Route path="/try-demo" component={ComingSoon}/>
      </div>
    </Switch>
  );
}

export default MainPage;