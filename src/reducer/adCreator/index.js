import {
  GET_AD_VIDEO_TASKS,
  GET_AD_VIDEO_TASKS_SUCCESS,
  GET_AD_VIDEO_TASKS_FAILED,
  GET_AD_VIDEO_SUCCESS,
  GET_AD_VIDEO_FAILED,
  GET_AD_VIDEO,
  UPLOAD_AD_VIDEO_IMAGE,
  UPLOAD_AD_VIDEO_IMAGE_SUCCESS,
  UPLOAD_AD_VIDEO_IMAGE_FAILED,
  UPLOAD_AD_VIDEO_SOUND,
  UPLOAD_AD_VIDEO_SOUND_SUCCESS,
  UPLOAD_AD_VIDEO_SOUND_FAILED,
  POST_VIDEO,
  POST_VIDEO_SUCCESS,
  POST_VIDEO_FAILED,
  GET_VIDEO,
  GET_VIDEO_SUCCESS,
  GET_VIDEO_FAILED,
  POST_SESSION,
  POST_SESSION_SUCCESS,
  POST_SESSION_FAILED,
  GET_SESSION2,
  GET_SESSION_SUCCESS,
  GET_SESSION_FAILED,
  GET_SCRIPT_SUGGESTION,
  GET_SCRIPT_SUGGESTION_SUCCESS,
  GET_SCRIPT_SUGGESTION_FAILED,
  GET_NOTIFICATION,
  GET_NOTIFICATION_SUCCESS,
  GET_NOTIFICATION_FAILED,
  DELETE_NOTIFICATION,
  DELETE_NOTIFICATION_SUCCESS,
  DELETE_NOTIFICATION_FAILED
} from "./constants";

const initialState = {
  video: {
    error: null,
    isLoading: false,
    payload: null
  },
  tasks: {
    error: null,
    isLoading: false,
    payload: null,
  },
  imageUpload: {
    error: null,
    isLoading: false,
    payload: null
  },
  soundUpload: {
    error: null,
    isLoading: false,
    payload: null
  },
  postVideoResult: {
    error: null,
    isLoading: false,
    payload: null
  },
  getVideoResult: {
    error: null,
    isLoading: false,
    payload: []
  },
  postSessionResult: {
    error: null,
    isLoading: false,
    payload: null
  },
  getSessionResult: {
    error: null,
    isLoading: false,
    payload: []
  },
  getScriptSuggestionResult: {
    error: null,
    isLoading: false,
    payload: []
  },
  getNotificationResult: {
    error: null,
    isLoading: false,
    payload: null
  },
  deleteNotificationResult: {
    error: null,
    isLoading: false,
    payload: null
  }
};

export default function (state = initialState, action) {
  switch (action.type) {
    case GET_AD_VIDEO: {
      return {...state, video: {error: null, isLoading: true}};
    }
    case GET_AD_VIDEO_SUCCESS: {
      return {...state, video: {error: null, isLoading: false, payload: action.payload}};
    }
    case GET_AD_VIDEO_FAILED: {
      return {...state, video: {payload: null, isLoading: false, error: action.payload}};
    }
    case GET_AD_VIDEO_TASKS: {
      return {...state, tasks: {error: null, isLoading: true}};
    }
    case GET_AD_VIDEO_TASKS_SUCCESS: {
      return {...state, tasks: {error: null, isLoading: false, payload: action.payload}};
    }
    case GET_AD_VIDEO_TASKS_FAILED: {
      return {...state, tasks: {payload: null, isLoading: false, error: action.payload}};
    }
    case UPLOAD_AD_VIDEO_IMAGE: {
      return {...state, imageUpload: {error: null, isLoading: true}};
    }
    case UPLOAD_AD_VIDEO_IMAGE_SUCCESS: {
      return {...state, imageUpload: {error: null, isLoading: false, payload: action.payload}};
    }
    case UPLOAD_AD_VIDEO_IMAGE_FAILED: {
      return {...state, imageUpload: {payload: null, isLoading: false, error: action.payload}};
    }
    case UPLOAD_AD_VIDEO_SOUND: {
      return {...state, soundUpload: {error: null, isLoading: true}};
    }
    case UPLOAD_AD_VIDEO_SOUND_SUCCESS: {
      return {...state, soundUpload: {error: null, isLoading: false, payload: action.payload}};
    }
    case UPLOAD_AD_VIDEO_SOUND_FAILED: {
      return {...state, soundUpload: {payload: null, isLoading: false, error: action.payload}};
    }
	case POST_VIDEO: {
      return {...state, postVideoResult: {error: null, isLoading: true}};
    }
    case POST_VIDEO_SUCCESS: {
      return {...state, postVideoResult: {error: null, isLoading: false, payload: action.payload}};
    }
    case POST_VIDEO_FAILED: {
      return {...state, postVideoResult: {payload: null, isLoading: false, error: action.payload}};
    }
	  case POST_SESSION: {
      return {...state, postSessionResult: {error: null, isLoading: true}};
    }
    case POST_SESSION_SUCCESS: {
      return {...state, postSessionResult: {error: null, isLoading: false, payload: action.payload}};
    }
    case POST_SESSION_FAILED: {
      return {...state, postSessionResult: {payload: null, isLoading: false, error: action.payload}};
    }
	case GET_SESSION2: {
      return {...state, getSessionResult: {error: null, isLoading: true, payload: initialState.getSessionResult.payload}};
    }
    case GET_SESSION_SUCCESS: {
      return {...state, getSessionResult: {error: null, isLoading: false, payload: action.payload}};
    }
    case GET_SESSION_FAILED: {
      return {...state, getSessionResult: {payload: [], isLoading: false, error: action.payload}};
    }
    case GET_VIDEO: {
      return {...state, getVideoResult: {error: null, isLoading: true, payload: initialState.getVideoResult.payload}};
    }
    case GET_VIDEO_SUCCESS: {
      return {...state, getVideoResult: {error: null, isLoading: false, payload: action.payload}};
    }
    case GET_VIDEO_FAILED: {
      return {...state, getVideoResult: {payload: [], isLoading: false, error: action.payload}};
    }
	  case GET_SCRIPT_SUGGESTION: {
      return {...state, getScriptSuggestionResult: {error: null, isLoading: true, payload: []}};
    }
    case GET_SCRIPT_SUGGESTION_SUCCESS: {
      return {...state, getScriptSuggestionResult: {error: null, isLoading: false, payload: action.payload}};
    }
    case GET_SCRIPT_SUGGESTION_FAILED: {
      return {...state, getScriptSuggestionResult: {payload: [], isLoading: false, error: action.payload}};
    }

    case GET_NOTIFICATION: {
      return {...state, getNotificationResult: {error: null, isLoading: true, payload: []}};
    }
    case GET_NOTIFICATION_SUCCESS: {
      return {...state, getNotificationResult: {error: null, isLoading: false, payload: action.payload}};
    }
    case GET_NOTIFICATION_FAILED: {
      return {...state, getNotificationResult: {payload: [], isLoading: false, error: action.payload}};
    }

    case DELETE_NOTIFICATION: {
      return {...state, deleteNotificationResult: {error: null, isLoading: true, payload: []}};
    }
    case DELETE_NOTIFICATION_SUCCESS: {
      return {...state, deleteNotificationResult: {error: null, isLoading: false, payload: action.payload}};
    }
    case DELETE_NOTIFICATION_FAILED: {
      return {...state, deleteNotificationResult: {payload: [], isLoading: false, error: action.payload}};
    }

    default:
      return state
  }
}