import React, { useState, useEffect } from 'react';
import { NavLink, Link } from 'react-router-dom';
import Logo from '../../assets/img/logo.png';
import Button from '../../components/Button';
import LoginMainPage from '../../components/LoginMainPage';
import PopupMainPage from '../../components/PopupMainPage';
import RegistrationMainPage from '../../components/RegistrationMainPage';

import './style.css';
import { useDispatch, useSelector } from 'react-redux';
import { logOut } from '../../reducer/auth/actions';

function HeaderMainPage() {
  const dispatch = useDispatch();
  const {payload} = useSelector(state => state.auth.login);
  const [isOpenPopup, setIsOpenPopup] = useState({
    status: false,
    type: null,
  });

  useEffect(() => {
    if (isOpenPopup.status && payload && payload.email) {
      setIsOpenPopup({...isOpenPopup, status: false});
    }
  }, [payload]);

  const handleClick = (type) => {
    if (type === isOpenPopup.type) {
      setIsOpenPopup({...isOpenPopup, status: !isOpenPopup.status});
    } else {
      setIsOpenPopup({status: true, type: type});
    }
  };

  const handleClickLogout = () => {
    dispatch(logOut());
  };

  return (
    <div>
      <div className="header-main-page-container">
        <div className="header-main-page-container-top">
          <div className="red-line"/>
          <div className="social-link-container">
            <Link>Facebook</Link>
            <Link>Instagram</Link>
            <Link>Twitter</Link>
            <Link>Youtube</Link>
            <div className="social-line"/>
          </div>
        </div>
        <div className="header-main-page-container-bottom">
          <div className="header-main-container-main">
            <div className="header-main-page-logo-container">
              <img src={Logo}/>
            </div>
            <div className="header-main-page-pages-container">
              <NavLink to="/why-vidnetics" activeClassName="active-main-link">Why Vidnetics?
                <div className="active-link-main"/>
              </NavLink>
              <NavLink to="/pricing" activeClassName="active-main-link">Pricing
                <div className="active-link-main"/>
              </NavLink>
              <NavLink to="/try-demo" activeClassName="active-main-link">Try Demo
                <div className="active-link-main"/>
              </NavLink>
            </div>
          </div>
          {payload && payload.email ?
            <div className="header-main-page-btn-container">
              <NavLink to="/main">Dashboard</NavLink>
              <Button className="red-btn logout-btn" onClick={handleClickLogout}>Logout</Button>
            </div> :
            <div className="header-main-page-btn-container">
              <Button className="sing-btn" onClick={() => handleClick('singin')}>SIGN IN</Button>
              <Button className="red-btn" onClick={() => handleClick('singup')}>SIGN UP</Button>
            </div>}
        </div>
      </div>
      <PopupMainPage type={isOpenPopup.type} isOpen={isOpenPopup.status} changeIsOpen={(status, type) => {
        setIsOpenPopup({status: status, type: type});
      }}>
        {isOpenPopup.type == 'singin' ? <LoginMainPage/> : <RegistrationMainPage/>}
      </PopupMainPage>
    </div>
  );
}

export default HeaderMainPage;