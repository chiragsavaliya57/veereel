import React, { useState, useCallback } from 'react';
import Icon from "@material-ui/core/Icon";
import './style.css';
import ReactPlayer from 'react-player';
import cx from "classnames";

const youtubeConfig = {
  theme: 'dark',
  modestbranding: 1,
  controls: 2,
  autohide: 2,
  showinfo: 0,
  showsearch: 0,
  rel: 1,
  iv_load_policy: 3,
  cc_load_policy: 1,
};

const playerConfig = {
  width: '120px',
  height: '60px',
  cc_load_policy: 1,
  class: 'pause',
};

function VideoInfoVideoSearch({id, info, className}) {

  return (
    <div className={cx("video-container", className)}>
      <div className="player-wrapper">
        <ReactPlayer
          className="react-player"
          url={'https://www.youtube.com/embed/' + id}
          width={playerConfig.width}
          light={true}
          height={playerConfig.height}
          youtubeConfig={{playerVars: youtubeConfig}}
        />
      </div>
      <div className="video-info">
        <div className="video-desc">{info}</div>
        <div className="video-panel">
          <Icon>desktop_windows</Icon>
          <span className="label">Chanel</span>
          <Icon>play_circle_outline</Icon>
          <Icon>info</Icon>
          <Icon>insert_drive_file</Icon>
          <i className="fa fa-tag"/>
        </div>
      </div>
    </div>
  );
}

export default VideoInfoVideoSearch;
