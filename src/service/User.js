import ApiService from './Api';

export default {
  getInfo() {
    return ApiService.get('/account/login/get.php');
  },
  changePassword(params) {
    return ApiService.post('/account/password/post.php', params);
  },
  changeInfo(params) {
    return ApiService.post('/account/details/post.php', params);
  }
};