import React from "react";

import Datetime from "react-datetime";

import InputAdornment from "@material-ui/core/InputAdornment";
import People from '@material-ui/icons/People';
import { makeStyles } from "@material-ui/core/styles";
import InputLabel from "@material-ui/core/InputLabel";
import FormControl from "@material-ui/core/FormControl";
import cx from "classnames";

import "./../../assets/scss/material-dashboard-pro-react.css"
import "./style.css"

function DateTimePicker({value, isValidDate, dateFormat, handleChange, timeFormat, closeOnSelect, placeholder, className, label}) {
  const style = {
    label: {
      color: "rgba(0, 0, 0, 0.26)",
      cursor: "pointer",
      display: "inline-flex",
      fontSize: "14px",
      transition: "0.3s ease all",
      lineHeight: "1.428571429",
      fontWeight: "400",
      paddingLeft: "0"
    }
  };

  const useStyles = makeStyles(style);
  const classes = useStyles();

  return (
    <div className={cx('date-time-picker', className)}>
      <InputLabel className={classes.label}>
        {label}
      </InputLabel>
      <br />
      <FormControl fullWidth={false}>
        <Datetime
          value={value}
          isValidDate={isValidDate}
          dateFormat={dateFormat}
          onChange={(e) => handleChange(e)}
          timeFormat={timeFormat}
          closeOnSelect={closeOnSelect}
          inputProps={{
            placeholder: placeholder
          }}
        />
      </FormControl>
    </div>
  );
}

export default React.memo(DateTimePicker);