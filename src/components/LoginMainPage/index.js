import React, { useState, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { auth, logOut } from '../../reducer/auth/actions';

import Button from '../../components/Button';
import Input from '../../components/Input';
import Loader from "../../components/Loader"
import ResetPassword from "../ResetPassword"


import "./style.css";

function LoginMainPage() {
  const dispatch = useDispatch();
  const [userInfo, setUserInfo] = useState({});
  const [isValid, setIsValid] = useState({
    email: true,
    password: true
  });
  const [isReset, setIsReset] = useState(false);
  const {error, isLoading} = useSelector(state => state.auth.login);

  const handleChangeEmail = (event) => {
    setUserInfo({...userInfo, email: event});
  };

  const handleChangePassword = (event) => {
    setUserInfo({...userInfo, password: event});
  };

  const handleSubmit = () => {
    const checkValid = {email: /^\w+\@\w+\.\w{2,4}$/.test(userInfo.email), password: /\w{3,}/.test(userInfo.password)};
    setIsValid(checkValid);

    if (checkValid.email && checkValid.password) {
      dispatch(auth(userInfo));
    }
  };

  return (
    <div>
      {
        isReset ?
          <ResetPassword />
          :
          <div className="login-main-page">
            <h1 className="login-main-page-header">Sign in</h1>
            <p className="login-main-page-error-container">{error}</p>
            <div className="login-main-page-input-container">
              <Input
                type="text"
                value={userInfo.email}
                onChange={handleChangeEmail}
                labelText={'Email'}
                className="white-input"
                error={!isValid.email}
                helperText={!isValid.email && "Email is not valid"}
              />
              <Input
                type="password"
                value={userInfo.password}
                onChange={handleChangePassword}
                labelText={'Password'}
                className="white-input"
                error={!isValid.password}
                helperText={!isValid.password && "Password is required"}
              />
            </div>
            <div className="login-main-page-btn-container">
              <Button className="login-main-page-btn-forgot" onClick={()=>{setIsReset(!isReset)}} disabled={isLoading}>Forgot password?</Button>
              <Button className="login-main-page-btn-login" onClick={handleSubmit} disabled={isLoading}>SIGN IN</Button>
            </div>
          </div>
      }
      {
        isLoading && <div className="login-main-page-loading" ><Loader className="loader-main-page" /></div>
      }
    </div>

  );
}

export default  React.memo(LoginMainPage);
