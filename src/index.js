import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import { Provider, useSelector } from 'react-redux';
import * as serviceWorker from './serviceWorker';
import { Switch, Route, Redirect, IndexRoute  } from 'react-router-dom';
import {ConnectedRouter} from 'connected-react-router';
import { createBrowserHistory } from "history";

import 'bootstrap/dist/css/bootstrap.min.css';
import createStore from './boot/createStore';
import MainPage from "./pages/MainPage"

import MainLayout from "layouts/Main.jsx";

const hist = createBrowserHistory();
const store = createStore(hist);

ReactDOM.render(<Provider store={store}>
  <ConnectedRouter history={hist}>
    <Switch>
      <PrivateRoute path='/main' component={MainLayout} />
      <Route path="/" component={MainPage}/>
    </Switch>
  </ConnectedRouter>
</Provider>, document.getElementById('root'));

function PrivateRoute({component: Component, ...rest}) {
  const {payload: payloadAuth} = useSelector(state => state.auth.login);

  return (<Route
    {...rest}
    render={props =>
      payloadAuth && payloadAuth.email ? (
        <Component {...props} />
      ) : (
        <Redirect path='/'/>
      )
    }
  />);
};


//ReactDOM.render(<Home/>, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
