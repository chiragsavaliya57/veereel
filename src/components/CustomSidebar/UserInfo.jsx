import React, {useState, useEffect} from "react";
import { NavLink } from "react-router-dom";
import "./style.css";
import { useSelector } from 'react-redux';
import avatar from "../../assets/img/faces/marc.jpg"
import ComingSoon from '../ComingSoon';

function UserInfo() {
  const {payload: payloadUserInfo} = useSelector(state => state.user.info);

  useEffect(()=>{
    if (payloadUserInfo) {
      // console.log('payloadUserInfo', payloadUserInfo);
    }
  }, [payloadUserInfo]);

  return (
    <div className="user-info-sidebar-container">
      <NavLink to={"/main/profile"} className="user-info-sidebar-container-link" activeClassName="user-info-link-active" name={"test"}>
        <div className="user-info-sidebar-container-link-avatar" style={{backgroundImage: `url(${avatar})`}} />
        <div className="user-info-sidebar-container-link-name">{payloadUserInfo ? payloadUserInfo.name : "Not logged in"}</div>
      </NavLink>
    </div>
  );
}

export default  React.memo(UserInfo);
