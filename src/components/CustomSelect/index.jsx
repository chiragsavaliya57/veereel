import React from 'react';
import cx from "classnames"

import { makeStyles } from "@material-ui/core/styles";

import FormControl from "@material-ui/core/FormControl";
import InputLabel from "@material-ui/core/InputLabel";
import Select from "@material-ui/core/Select";
import MenuItem from "@material-ui/core/MenuItem";

import styles from "../../assets/jss/material-dashboard-pro-react/customSelectStyle";
import "./style.css";
const useStyles = makeStyles(styles);


function CustomSelect({value, options, handleChange, label, multiple, className}) {
  const classes = useStyles();

  return (
      <FormControl fullWidth className={cx([classes.selectFormControl, "cust-select", className])}>
        <InputLabel
          htmlFor="multiple-select"
          className={classes.selectLabel}
        >
          {label}
        </InputLabel>
        <Select
          multiple={multiple}
          value={value}
          onChange={(e, v) => handleChange(e.target.value, v.props.children)}
          MenuProps={{
            className: classes.selectMenu,
            classes: { paper: classes.selectPaper }
          }}
          classes={{ select: classes.select }}
          inputProps={{
            name: "multipleSelect",
            id: "multiple-select",
            className: "test"
          }}
        >
          {
            options.map((opt, key)=> (<MenuItem
              classes={{
                root: cx([classes.selectMenuItem,"cust-option"]),
                selected:"cust-option-selected"
              }}
              value={key}
            >{opt}</MenuItem>))
          }

        </Select>
      </FormControl>
  );
}

export default  React.memo(CustomSelect);
