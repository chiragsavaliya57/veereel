import React from "react";

import Sidebar from "components/Sidebar/Sidebar.jsx";


import routes from "routes.js";


import image from "assets/img/sidebar-2.jpg";
import logo from "assets/img/logo-white.svg";

class CSideBar extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      mobileOpen: false,
      miniActive: false,
      image: image,
      color: "blue",
      bgColor: "black"
    };
  }
  handleDrawerToggle = () => {
    this.setState({ mobileOpen: !this.state.mobileOpen });
  };
  render() {
    return (
        <Sidebar
          routes={routes}
          logoText={"Creative Tim"}
          logo={logo}
          image={this.state.image}
          handleDrawerToggle={this.handleDrawerToggle}
          open={this.state.mobileOpen}
          color={this.state.color}
          bgColor={this.state.bgColor}
          miniActive={this.state.miniActive}
        />
    );
  }
}

export default CSideBar;