import React, { useState, useMemo, useEffect } from 'react';
import './style.css';
import cx from 'classnames';

function CustomTable({tableHead, tableData, className}) {

  return (
    <div className={cx('table', className)}>
      {tableHead &&
      <div className="table-header table-row">
        {tableHead.map((i) => (
          <div className={cx('header-column column', i.className)}>
            {i.content}
          </div>
        ))}
      </div>
      }
      <div>
        {tableData && tableData.length === 0 && (
          <div className="table-empty">Nothing to display</div>
        )}
        {tableData.map((row, key) => (
          <div key={key} className="table-body-row table-row">
            {row.map((item) => (
              <div className={cx('column', item.className)}>{item.content}</div>
            ))}
          </div>
        ))}
      </div>
    </div>
  );
}

export default CustomTable;
