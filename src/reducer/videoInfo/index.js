import {
  GET_VIDEO,
  GET_VIDEO_SUCCESS,
  GET_VIDEO_FAILED,
  FIND_VIDEO,
  FIND_VIDEO_SUCCES,
  FIND_VIDEO_FAILED
} from "./constants";

const initialState = {
  error: null,
  isLoading: false,
  payload: [],
};

export default function (state = initialState, action) {
  switch (action.type) {
    case GET_VIDEO_SUCCESS: {
      return {...state, error: null, isLoading: true};
    }
    case FIND_VIDEO: {
      return {...state, error: null, isLoading: true};
    }
    case FIND_VIDEO_SUCCES: {
      return {...state, error: null, isLoading: false, payload: action.payload};
    }
    case FIND_VIDEO_FAILED: {
      return {...state, error: action.payload, isLoading: false};
    }
    default:
      return state
  }
}
