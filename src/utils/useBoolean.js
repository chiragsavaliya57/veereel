import {useState, useCallback} from "react";

export default function useBoolean(initialState) {
  const [value, setValue] = useState(initialState);

  return {
    value,
    setValue,
    setTrue: useCallback(() => setValue(true)),
    setFalse: useCallback(() => setValue(false)),
    toggleValue: useCallback(() => setValue(!value), [value])
  }
}
