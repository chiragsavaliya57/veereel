import React from 'react';
import PropTypes from "prop-types";
import { connect } from 'react-redux';

// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import ListItemText from '@material-ui/core/ListItemText';

// @material-ui/icons

// core components
import CustomizedSnackbars from "components/Snackbar/CustomizedSnackbars";
import GridContainer from "components/Grid/GridContainer.jsx";
import GridItem from "components/Grid/GridItem.jsx";
import regularFormsStyle from "assets/jss/material-dashboard-pro-react/views/regularFormsStyle";
import Videoicon from "assets/img/pro-video.png";
import icon5 from "assets/img/icon-5.png";
import icon7 from "assets/img/icon-7.png";
import icon8 from "assets/img/icon-8.png";
import icon9 from "assets/img/icon-9.png";
import './style.css'
import { getAdVideoTasks } from '../../reducer/adCreator/actions';
import { setNotificationMessage } from '../../reducer/adNotification/actions';
import ReactPlayer from 'react-player';
import { Link } from 'react-router-dom';
import Loader from "../../components/Loader";

// documnetation link for dropzone https://www.npmjs.com/package/material-ui-dropzone

const stats = [
  { name: "Videos", src: icon5, value: ({ videos }) => videos ? videos.length : 0 },
  { name: "Images", src: icon7, value: ({ images }) => images ? images.length : 0 },
  { name: "Sounds", src: icon8, value: ({ sounds }) => sounds ? sounds.length : 0 },
  { name: "Text", src: icon9, value: ({ text }) => text ? text.length : 0 },
];

class AdCreator extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isnotificationopen: false
    }

  }
  componentDidMount() {
    this.props.getAdVideoTasks()
    this.setState({ isnotificationopen: true });
    setTimeout(() => {
      this.saveNotification();
    }, 500);
  }

  saveNotification = () => {
    const { isnotificationopen } = this.state
    if (isnotificationopen) {
      this.props.setNotificationMessage('This is a notification with close button.')
    }
  }

  render() {
    const { payload: tasks, loading } = this.props.tasks;
    const { isnotificationopen } = this.state;
    return (
      <GridContainer justify="center" alignItems="center" className="v-ad-creator">
        <React.Fragment>
          <h2 className='sub-title'>Create video ad from template</h2>
          <div className="video-teb">

            <GridContainer>
              {/* {isnotificationopen && */}
              <CustomizedSnackbars
                open={isnotificationopen}
                onclose={(val) => this.setState({ isnotificationopen: val })}
                message={"This is a notification with close button."}
              />
              {/* } */}
              {tasks
                ? tasks.map(task => (
                  <GridItem xs={12} sm={12} md={6} lg={4} key={task.taskId}>
                    <div className="cover-div">
                      <div className="video-cover">
                        <ReactPlayer url={task.template} width="100%" height="100%" controls={true} />
                      </div>
                      <div className="video-content">
                        <Link to={{ pathname: `/main/ad-creator/editor`, state: { task } }}
                          title={task.name}
                          className="title-text">

                          {task.name}
                        </Link>
                        <div className="btn-content">

                          <List className="dtl-list">
                            {stats.map(stat => (
                              <ListItem key={stat.name}>
                                <ListItemAvatar>
                                  <img src={stat.src} alt={stat.name} />

                                </ListItemAvatar>
                                <ListItemText
                                  primary={stat.value(task)}
                                />
                              </ListItem>
                            ))}
                          </List>
                          <Link to={{ pathname: `/main/ad-creator/editor`, state: { task } }} title="Select"
                            className="btn">Select</Link>

                        </div>
                      </div>
                    </div>
                  </GridItem>
                ))
                : <Loader />
              }
            </GridContainer>
          </div>
        </React.Fragment>
      </GridContainer>
    );
  }
}

AdCreator.propTypes = {
  classes: PropTypes.object
};

const mapStateToProps = ({ adCreator: { tasks }, adCreator: { scriptsuggestion } }) => ({ tasks, scriptsuggestion });


const mapDispatchToProps = dispatch => ({
  getAdVideoTasks: () => dispatch(getAdVideoTasks()),
  setNotificationMessage: message => dispatch(setNotificationMessage(message))
});

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(regularFormsStyle)(AdCreator));
