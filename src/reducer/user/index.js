import {
  GET_USER_START,
  GET_USER_SUCCESS,
  GET_USER_FAILED,
  CHANGE_PASSWORD_START,
  CHANGE_PASSWORD_SUCCESS,
  CHANGE_PASSWORD_FAILED,
  CHANGE_INFO_START,
  CHANGE_INFO_SUCCESS,
  CHANGE_INFO_FAILED,
} from './constants';

const initialState = {
  info: {
    error: null,
    isLoading: false,
    payload: null,
  },
  changePassword: {
    error: null,
    isLoading: false,
    payload: null,
  },
  changeInfo: {
    error: null,
    isLoading: false,
    payload: null,
  },
};

export default function (state = initialState, action) {
  switch (action.type) {
    case GET_USER_START: {
      return {...state, info: {payload: null, isLoading: true, error: null}};
    }
    case GET_USER_SUCCESS: {
      return {...state, info: {payload: action.payload, isLoading: false, error: null}};
    }
    case GET_USER_FAILED: {
      return {...state, info: {payload: false, isLoading: false, error: action.payload}};
    }
    case CHANGE_PASSWORD_START: {
      return {...state, changePassword: {payload: null, isLoading: true, error: null}};
    }
    case CHANGE_PASSWORD_SUCCESS: {
      return {...state, changePassword: {payload: true, isLoading: false, error: null}};
    }
    case CHANGE_PASSWORD_FAILED: {
      return {...state, changePassword: {payload: false, isLoading: false, error: action.payload}};
    }
    case CHANGE_INFO_START: {
      return {...state, changeInfo: {payload: null, isLoading: true, error: null}};
    }
    case CHANGE_INFO_SUCCESS: {
      return {
        ...state,
        changeInfo: {payload: action.payload, isLoading: false, error: null},
        info: {...state.info, payload: action.payload},
      };
    }
    case CHANGE_INFO_FAILED: {
      return {...state, changeInfo: {payload: false, isLoading: false, error: action.payload}};
    }
    default:
      return state;
  }
}
