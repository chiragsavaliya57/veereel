import React from 'react';
import './Home.css';
import axios from 'axios';
import Table from "./CustomTable/Table.jsx";
import DemoTables from "./DemoTables/DemoTables.jsx";
import DemoTabs from "./DemoTabs/DemoTabs.jsx";
import CSideBar from "./CSideBar/CSideBar.js";

import routes from "routes.js";
import { Switch, Route, Redirect } from "react-router-dom";

class Home extends React.Component {
	
	constructor(props) {
		super(props);
		this.state = {data: "notthing"};
	}
	componentWillMount() {
		axios
			.get(document.location.origin+"/api/getLogin.php")
			.then(({ data }) => {
				console.log(data);
				this.setState({
					isLoggedIn: data['isLoggedIn'],
					isOAuth2: data['isOAuth2']
				});
				if(this.state.isLoggedIn!=1) {
					window.location.replace("/login");
				} else
				if(this.state.isOAuth2!=1) {
					window.location.replace("/api/oauth2callback.php");
				}
			});
		
		
	}	
	getRoute = () => {
    return window.location.pathname !== "/admin/full-screen-maps";
  };
	getRoutes = routes => {
    return routes.map((prop, key) => {
      if (prop.collapse) {
        return this.getRoutes(prop.views);
      }
      if (prop.layout === "/admin") {
        return (
          <Route
            path={prop.layout + prop.path}
            component={prop.component}
            key={key}
          />
        );
      } else {
        return null;
      }
    });
  };

	render() {
		const { classes, ...rest } = this.props;
		  return (

			<div className="App">
				<div >
					<CSideBar />
					<div className={classes.content}>
						<div className={classes.container}>
						<Switch>
						  {this.getRoutes(routes)}
						  <Redirect from="/admin" to="/admin/dashboard" />
						</Switch>
						</div >
					</div>
		  
				</div>
				<div className="Center">
					<DemoTabs/>
					<DemoTables/>
				</div>
			</div>
		  );
	}
}

export default Home;