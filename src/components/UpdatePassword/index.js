import React, { useState, useEffect } from 'react';
import Input from '../Input';
import Button from '../Button';
import Alert from '../Alert';

import { changePassword } from '../../reducer/user/actions';
import { useDispatch, useSelector } from 'react-redux';

import cx from 'classnames';
import './style.css';

function UpdatePassword() {
  const dispatch = useDispatch();
  const {error, payload, isLoading} = useSelector(state => state.user.changePassword);
  const [isValid, setIsValid] = useState({
    old: true,
    new: true,
    comfirm_new: true,
    same: true,
  });

  const [alertInfo, setAlertInfo] = useState({
    text: '',
    type: '',
    isOpen: false,
  });

  const [passwordInfo, setPasswordInfo] = useState({
    old: '',
    new: '',
    comfirm_new: '',
  });

  const handleChangeInputs = (type, e) => {
    setPasswordInfo({...passwordInfo, [type]: e});
  };

  useEffect(() => {
    if (payload) {
      setPasswordInfo({
        old: '',
        new: '',
        comfirm_new: '',
      });
    }
  }, [payload]);

  const handleClick = () => {
    const checkValid = {
      old: !!passwordInfo.old && !!passwordInfo.old.length,
      new: /\w{3,}/.test(passwordInfo.new),
      comfirm_new: /\w{3,}/.test(passwordInfo.comfirm_new),
      same: passwordInfo.new === passwordInfo.comfirm_new,
    };


    setIsValid(checkValid);
    if (checkValid.old && checkValid.new && checkValid.comfirm_new && checkValid.same) {
      dispatch(changePassword({
        oldPassword: passwordInfo.old,
        newPassword: passwordInfo.new,
      }))
        .then(() => setAlertInfo({text: 'Password updated successfully', type: 'success', isOpen: true}))
        .catch(() => setAlertInfo({text: 'Temporary problems try again later', type: 'error', isOpen: true}));
    }
  };

  return (
    <div className="update-password-container">
      <Input
        type="password" value={passwordInfo.old}
        onChange={(e) => handleChangeInputs('old', e)}
        placeholder={'Enter Old Password'}
        labelText={'Old Password'}
        error={!isValid.old}
        helperText={!isValid.old && 'Old password is required'}
      />
      <Input
        type="password" value={passwordInfo.new}
        onChange={(e) => handleChangeInputs('new', e)}
        placeholder={'Enter New Password'}
        labelText={'New Password'}
        error={!isValid.new || !isValid.same}
        helperText={(!isValid.new && 'New password is required') || (!isValid.same && 'New password and confirm the new must be the same')}
      />
      <Input
        type="password" value={passwordInfo.comfirm_new}
        onChange={(e) => handleChangeInputs('comfirm_new', e)}
        placeholder={'Enter Confirm New Password'}
        labelText={'Confirm New Password'}
        error={!isValid.comfirm_new || !isValid.same}
        helperText={(!isValid.comfirm_new && 'Confirm new password is required') || (!isValid.same && 'New password and confirm the new must be the same')}
      />
      <Button className="change-password-btn" onClick={handleClick} disabled={isLoading}>Change</Button>
      <div className="alert-update-password">
        <Alert
          message={alertInfo.text}
          isOpen={alertInfo.isOpen}
          theme={alertInfo.type}
          handlerClose={() => setAlertInfo({...alertInfo, isOpen: false})}
        />
      </div>
    </div>
  );
}

export default UpdatePassword;