import React from 'react';
import axios from 'axios';
import './VideoEditor.css';
import ImageUpload from "components/ImageUpload/ImageUpload.js";
// import Accordion from "components/VideoEditor/Accordion/Accordion.jsx";
import CustomInput from 'components/VideoEditor/CustomInput/CustomInput.jsx';
import CustomLinearProgress from 'components/CustomLinearProgress/CustomLinearProgress.jsx';

class VideoEditor extends React.Component {
	
  constructor(props) {
    super(props);
    this.state = {videoSrc: 'videos/test.webm', answer:'test', changes:{text:[]}, percent:0, id:'', jobs:[], extension:'mp4'};
	this.handleLoadVideo = this.handleLoadVideo.bind(this);
	this.handleCheckVideo = this.handleCheckVideo.bind(this);
	this.handleCheckAllVideo = this.handleCheckAllVideo.bind(this);
	this.CheckAllVideo = this.CheckAllVideo.bind(this);
	this.handleCheckAllVideoServer = this.handleCheckAllVideoServer.bind(this);
	this.handleChangeText = this.handleChangeText.bind(this);
	this._handleSubmitImageUpload = this._handleSubmitImageUpload.bind(this);
	this.timer = this.timer.bind(this);
	this.timer2 = this.timer2.bind(this);
	this.timer3 = this.timer3.bind(this);
	this.videoSrc ='';
  }
  
  componentDidMount() {
    var parObj = this;
	axios
		.get(document.location.origin+"/api/videos/jobs/get.php?id=1")
		.then(({ data }) => {
			var changes2 = parObj.state.changes;
			changes2.text = data.text;
			parObj.setState({changes: changes2});
		});
	this.timer3();
  }
  

  componentWillUnmount() {
    clearInterval(this.timerId);
  }
  
  timer3() {
		this.CheckAllVideo(1);
		this.timerId2 = setTimeout(this.timer3, 10000);
	}
  
  /*timer() {
    var parObj = this;
	var video = document.getElementById('video');
	axios
		.get(document.location.origin+"/api/videos/getVideoPercent.php")
		.then(({ data }) => {
			console.log(data);
			parObj.setState({ percent: data.percent });
			
			if(data.video == 0) {
				this.timerId = setTimeout(this.timer, 1000);
			} else {
				parObj.setState({videoSrc: data.video});
				video.load();
			}
		});
  }*/
  
  timer() {
    var parObj = this;
	if(parObj.state.id!='') {
		axios
			.get(document.location.origin+"/api/videos/get.php?id="+encodeURIComponent(parObj.state.id)+"&extension="+encodeURIComponent(parObj.state.extension))
			.then(({ data }) => {
				var json = data;
				console.log(json);
				//parObj.setState({ percent: data.percent });
				
				if(json.state != "finished") {
					this.timerId = setTimeout(this.timer, 2000);
				} else {
					//var index = json.template.dest.lastIndexOf("\\");
					//var filename = json.output.substring(0, index);
					
					var pathToFile = "videos/" + json.endResult;
					console.log(pathToFile);
					//parObj.videoSrc = pathToFile;
					//this.timerId = setTimeout(this.timer2, 600);
					var video = document.getElementById('video');
					parObj.setState({videoSrc: pathToFile});
					video.load();
					
				}
			});
	}
  }
  
timer2() {
	var video = document.getElementById('video');
	console.log("tick");
	this.setState({videoSrc: this.videoSrc});
	video.load();
}
  
  
  handleLoadVideo(event) {
	  
	var parObj = this;
	var video = document.getElementById('video');
   /* axios
		.get(document.location.origin+"/api/videos/post.php?text="+encodeURIComponent(parObj.state.text)+"&extension="+encodeURIComponent(parObj.state.extension))
		.then(({ data }) => {
			//parObj.setState({videoSrc: data.videoSrc});
			//parObj.setState({answer: data.answer});
			//video.load();
			parObj.setState({ id: data });
			console.log(data);
			this.timerId = setTimeout(this.timer, 2000);
		});*/
	axios
	.post(document.location.origin+"/api/videos/post.php", {
		extension: encodeURIComponent(parObj.state.extension),
		changes: JSON.stringify(parObj.state.changes)
	  })
	.then(({ data }) => {
		//parObj.setState({videoSrc: data.videoSrc});
		//parObj.setState({answer: data.answer});
		//video.load();
		parObj.setState({ id: data });
		console.log(data);
		this.timerId = setTimeout(this.timer, 2000);
	});
  }
  handleCheckVideo(event) {
	
	var parObj = this;
	var video = document.getElementById('video');
	if(parObj.state.id!='') {
		axios
			.get(document.location.origin+"/api/videos/get.php?id="+encodeURIComponent(parObj.state.id)+"&extension="+encodeURIComponent(parObj.state.extension))
			.then(({ data }) => {
				//parObj.setState({videoSrc: data.videoSrc});
				//parObj.setState({answer: data.answer});
				//video.load();
				console.log(data);
				//this.timerId = setTimeout(this.timer, 1000);
			});
	}
  }
  
  CheckAllVideo(doChecks) {
	
	var parObj = this;
	var video = document.getElementById('video');
    axios
		.get(document.location.origin+"/api/videos/get.php?confirm="+doChecks)
		.then(({ data }) => {
			console.log(data);
			parObj.setState({ jobs: data });
		});
  }
  _handleSubmitImageUpload(e, elem) {
	  e.preventDefault();
	  console.log(elem);
	  console.log(elem.state.file);
	  console.log(elem.state.imagePreviewUrl);
	  var f = elem.state.file;
	  var ext = f.name.substring(f.name.indexOf(".")+1);
	  axios
		.post(document.location.origin+"/api/videos/images/post.php", {
			blob:elem.state.imagePreviewUrl,
			extension:ext
		  })
		.then(({ data }) => {
			console.log(data);
		});
  }
  
  handleCheckAllVideo(event) {
	
	this.CheckAllVideo(0);
  }
  
  handleCheckAllVideoServer(event) {
	
	var parObj = this;
	var video = document.getElementById('video');
    axios
		.get(document.location.origin+"/api/videos/get.php?server=none")
		.then(({ data }) => {
			console.log(data);
		});
  }
  
  handleChangeText(event) {
	  var changes2 = this.state.changes;
	  changes2.text[event.target.index] = event.target.value;
	  this.setState({changes: changes2});
  }

  /*
  <CustomInput
									id="regular"
									inputProps={{
									  placeholder: "Regular"
									}}
									formControlProps={{
									  fullWidth: true
									}}
								  />
  */
  
  render() {
    return (
	
		  
	<div className="VideoEditorContainer">
	<div className="VideoEditorButton">
		<button onClick={this.handleLoadVideo}>Load</button>
		<button onClick={this.handleCheckVideo}>Check</button>
		<button onClick={this.handleCheckAllVideo}>Check All</button>
		<button onClick={this.handleCheckAllVideoServer}>Check All Server</button>
	</div>
	
		<div className="VideoEditor">
			<div className="VideoEditorAccordion">
				<Accordion 
						  active={0}
						  collapses={[
							{
							  title: "Text",
							  content:this.state.changes.text.map((item, index) => <input type="text" value={this.state.changes.text[index]} index={index} onChange={this.handleChangeText} /> )
								 
							},
							{
							  title: "Image",
							  content:<form enctype="multipart/form-data" action={document.location.origin+"/api/videos/images/post.php"} method="POST">
										<input type="hidden" name="MAX_FILE_SIZE" value="1000000" />
										Отправить этот файл: <input name="userfile" type="file" />
										<input type="submit" value="Отправить файл" />
									</form>
								},
							{
							  title: "Audio",
							  content:<ImageUpload _handleSubmitImageUpload={this._handleSubmitImageUpload}/>}
						  ]}
						/>
						</div>
			
		  <div className="VideoEditorVideo">
				
			  <video id="video" className="Video" controls width="600">
				  <source src={this.state.videoSrc} type="video/webm"/>
				</video>
			</div>
			 < CustomLinearProgress
        variant = "determinate"
        color = "info"
        value ={this.state.percent}
      />
		</div>
		<div>
		<table>
		<th>Uid</th><th>state</th><th>Started at</th><th>Finished at</th><th>Link</th>
		{this.state.jobs.map((item) => 
			<tr><td>{item.uid}</td><td>{item.state}</td><td>{item.started}</td><td>{item.finished}</td><td><a href={"videos/" + item.uid + '.' + item.extension}>Link</a></td></tr>
		)}
		</table>
		</div>
		
	</div>
    );
  }

}

export default VideoEditor;

/*
<source src="video.mp4" type="video/mp4"><!-- MP4 для Safari, IE9, iPhone, iPad, Android, и Windows Phone 7 -->
		  <source src="video.webm" type="video/webm"><!-- WebM/VP8 для Firefox4, Opera, и Chrome -->
		  <source src="video.ogv" type="video/ogg"><!-- Ogg/Vorbis для старых версий браузеров Firefox и Opera -->
		  <object data="video.swf" type="application/x-shockwave-flash"><!-- добавляем видеоконтент для устаревших браузеров, в которых нет поддержки элемента video -->
			<param name="movie" value="video.swf">
		  </object>*/