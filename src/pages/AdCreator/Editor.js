import React from 'react';
import PropTypes from "prop-types";
import { DropzoneArea } from 'material-ui-dropzone';
import { connect } from 'react-redux';

// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";
import InputAdornment from "@material-ui/core/InputAdornment";
import TextField from '@material-ui/core/TextField';
import Fab from '@material-ui/core/Fab';

// @material-ui/icons
import Icon from '@material-ui/core/Icon';
import CloseIcon from '@material-ui/icons/Close';
import InsertDriveFile from '@material-ui/icons/InsertDriveFile';
import AddIcon from '@material-ui/icons/Add';
import WbIncandescentIcon from '@material-ui/icons/WbIncandescent';
import ArrowForwardIcon from '@material-ui/icons/ArrowForward';
import VisibilityOffIcon from '@material-ui/icons/VisibilityOff';

// core components
import Accordion from "components/Accordion/Accordion.jsx";
import GridContainer from "components/Grid/GridContainer.jsx";
import CustomInput from "components/CustomInput/CustomInput.jsx";
import Card from "components/Card/Card.jsx";
import CardHeader from "components/Card/CardHeader.jsx";
import CustomDropdown from 'components/CustomDropdown/CustomDropdown.jsx';

import regularFormsStyle from "assets/jss/material-dashboard-pro-react/views/regularFormsStyle";
import icon9 from "assets/img/icon-9.png";
import icon12 from "assets/img/icon-12.png";
import icon15 from "assets/img/icon-15.png";
import videoImg from "../../assets/img/video-img.png";
import imgInput from "../../assets/img/img-input.png";
import videoPath from "../../assets/img/video-path.png";
import './style.css'
import { getAdVideo, uploadAdVideoImage, postVideo, getVideo, getScriptSuggestion,postSession, postNotification,getNotification } from '../../reducer/adCreator/actions';


import SuggestionsModal from "./SuggestionsModal";
import ReactPlayer from "react-player";
import { Redirect } from 'react-router-dom';
import { inputImage, inputSound } from "../../utils";
import AdsCreatorService from "../../service/AdsCreator";
import WaveForm from '../../components/WaveForm';
import Loader from '../../components/Loader';

// documnetation link for dropzone https://www.npmjs.com/package/material-ui-dropzone

const count = [0, 1, 2, 3, 4];

const ColorButton = withStyles(() => ({
  root: {
    color: '#fff',
    backgroundColor: '#ff3d00',
    borderRadius: '17px !important',
    padding: '8px 15px !important',
    lineHeight: '5.6px',
    fontSize: '10px',
    textTransform: 'uppercase',
    letterSpacing: '2.24px',
    height: '22px !important',
    '&:hover': {
      backgroundColor: '#fff',
      color: '#ff3d00 !important',
    },
  },
}))(Fab);



class AdCreator extends React.Component {

  constructor(props) {
    super(props);
    const routerState = this.props.routerState;
    if (!(routerState && routerState.task)) return;
    const task = routerState.task;
    // console.log(task);
    this.state = {
      videoName: task.name,
      sessionUuid: null,
      taskId: task.taskId,
      template: task.template,
      suggestionScript: [],
      categories: [],
      isModalOpen: false,
      isEditName: false,
      listClassnames: {
        text: '',
        image: '',
        video: '',
        audio: ''
      },
      videos: task.videos,
      images: task.images,
      sounds: task.sounds,
      text: task.text,
	  initialText: task.initialText,
	  imageSizes:task.imageSizes,
      loadingImages: task.images.map(() => false),
      loadingSounds: task.sounds.map(() => false)
    };
    this.handleScriptSuggestion();
    this.handleCategoryData();
  }

  componentWillUnmount () {
	  clearInterval(this.timerId);
	  clearInterval(this.timerId2);

  }

  timerTick = () => {

    if (!this.props.getVideoResult.isLoading && this.props.getVideoResult.payload != null && this.props.postVideoResult.payload != null) {
      var item = this.props.getVideoResult.payload.find((item) => item.uid == this.props.postVideoResult.payload);
      if (item) {
        if (item.state == "queued") {
          console.log("queued");
        }
        if (item.state == "finished") {
          console.log("finished");
          clearInterval(this.timerId);
          this.setState({
            template: 'https://anabasis.online/main/videos/' + item.uid + '.' + item.extension
          });

        }
      }
    }

    if (!this.props.postVideoResult.isLoading && !this.props.getVideoResult.isLoading) {
      this.props.getVideo();
    }

  }

  timerTick2 = () => {

    if (!this.props.postSessionResult.isLoading) {
		clearInterval(this.timerId2);
		document.location.replace('../my-videos');
    }

  }

  handleArrayChange(name, index, value) {
    this.setState(({ [name]: values }) => ({ [name]: [...values.slice(0, index), value, ...values.slice(index + 1)] }))
  }

  handleTextChange(index, text) {
    this.handleArrayChange('text', index, text);
  }

  handleImageChange(index) {
    inputImage(image => {
      this.handleArrayChange('loadingImages', index, true);
      AdsCreatorService.uploadImage(image, this.state.imageSizes[index]).then(url => {
        this.handleArrayChange('images', index, `https://www.anabasis.online/main/images/${url}`);
      }).finally(() => this.handleArrayChange('loadingImages', index, false));
    });
  }

  handleScriptSuggestion() {
    AdsCreatorService.getScriptSuggestion().then(res => {
      this.setState({ suggestionScript: res })
    }).finally(() => { });
  }

  
  handleCategoryData() {
    AdsCreatorService.getCategory().then(res => {
      this.setState({ categories: res })
    }).finally(() => { });
  }

  handleSoundChange(index) {
    inputSound(sound => {
      this.handleArrayChange('loadingSounds', index, true);
      AdsCreatorService.uploadSound(sound).then(url => {
        this.handleArrayChange('sounds', index, `https://www.anabasis.online/main/audio/${url}`);
      }).finally(() => this.handleArrayChange('loadingSounds', index, false));
    });
  }

  handleMouseEnter = name => {
    this.setState(prevState => ({
      listClassnames: {
        ...prevState.listClassnames,
        [name]: 'active'
      }
    }))
  };

  handleMouseLeave = () => {
    this.setState({
      listClassnames: {
        text: '',
        image: '',
        video: '',
        audio: ''
      }
    });
  };

  handleClickRender = (e) => {
    this.props.postVideo({ sessionUuid:this.state.sessionUuid,taskId: this.state.taskId, name: this.state.videoName, extension: "mp4", changes: { text: this.state.text, images: this.state.images, sounds: this.state.sounds } });
    clearInterval(this.timerId);
    //console.log({message:this.state.videoName + " is in queue.",type:  0});
    this.props.postNotification({message:this.state.videoName + " is in queue.",type:  2});
    //console.log(this.props.getNotification())
    this.timerId = setInterval(this.timerTick, 5000);
  };

  handleClickSave = (e) => {
    this.props.postSession({ sessionUuid:this.state.sessionUuid, taskId: this.state.taskId, name: this.state.videoName, changes: { text: this.state.text, images: this.state.images, sounds: this.state.sounds,
		initialText: this.state.initialText,
		imageSizes:this.state.imageSizes,
	} });
	this.timerId2 = setInterval(this.timerTick2, 500);

  };

  handleInput = (e) => {
    this.setState({ [e.target.name]: e.target.value });
    if (e.target.name === "videoName") this.setState({ isEditName: true })
  };

  handleModalClose = (res) => {
    this.setState({ isModalOpen: false, text: res.text });
  };

  handleModalOpen = e => {
    e.stopPropagation();
    this.setState({ isModalOpen: true });
  };

  render() {
    if (!(this.props.routerState && this.props.routerState.task)) return <Redirect to="/ad-creator" />;
    const { videoName, isEditName, isModalOpen, suggestionScript, categories } = this.state;    
    const postVideoId = this.props.postVideoResult.payload;

    return (
      <GridContainer className="v-ad-creator">
        <React.Fragment>
          <SuggestionsModal
            open={isModalOpen}
            suggestionScript={suggestionScript}
            categories={categories}
            onClose={this.handleModalClose}
          />
          <Card className="v-custom-card custom-card">
            <CardHeader className="sub-title-cover">
              <div className="header-card">
                <div className='text-input'>
                  <TextField
                    id="standard-name"
                    className='headertext'
                    margin="normal"
                    name="videoName"
                    value={videoName}
                    onChange={this.handleInput.bind(this)}
                  />
                  {!isEditName && <Icon>edit</Icon>}
                  {isEditName && <Icon className="save">save</Icon>}
                </div>
                <div className="link-btn">
                  <button onClick={this.handleClickSave} className="btn" title="Save & continue later">Save &amp; continue later</button>&nbsp;&nbsp;
                  <button onClick={this.handleClickRender} className="btn">Render<span className="img-cover">
                    <img src={icon12} alt="" /></span>
                  </button>
                </div>
              </div>
            </CardHeader>
          </Card>
          <Card className="v-custom-card">
            <div className="v-import-block">
              <div className="v-import-media-block v-accordian">
                <Accordion
                  collapses={[
                    {
                      title: <div key="text" className={this.state.listClassnames.text}
                        onMouseEnter={() => this.handleMouseEnter("text")}
                        onMouseLeave={this.handleMouseLeave.bind(this)}>
                        <p><Icon>text_rotation_none</Icon>Text</p>
                        <div className="tooltip-box">
                          <div className="tooltip-cover">
                            <h3>STEP 1</h3>
                            <p>Start by adding some text</p>
                          </div>
                        </div>
                      </div>,
                      extra: <ColorButton
                        variant="contained"
                        color="primary"
                        size="small"
                        className="extraButton"
                        onClick={this.handleModalOpen}
                      >
                        Script Suggestions <InsertDriveFile />
                      </ColorButton>,
                      content:
                        <div className="v-import-text">
                          <ol className="v-import-text-ul">
                          {this.state.text.map((item,index) => (

                              <li key={index.toString()}>
                                <CustomInput
                                  id="regular"
                                  inputProps={{
                                    inputProps: {maxLength:this.state.initialText[index].length},
                                    endAdornment: (
                  <InputAdornment position="end">{this.state.text[index].length}/{this.state.initialText[index].length}</InputAdornment>),

                                    onChange: ({ target: { value } }) => this.handleTextChange(index, value),
                                    value: this.state.text[index] || ''
                                  }}
                                  formControlProps={{
                                    fullWidth: true
                                  }}
                                />
                              </li>
                            ))}
                          </ol>
                        </div>
                    },
                    {
                      title: <div key="image" className={this.state.listClassnames.image}
                        onMouseEnter={() => this.handleMouseEnter("image")}
                        onMouseLeave={this.handleMouseLeave.bind(this)}>
                        <p><Icon>photo</Icon>Image</p>
                        <div className="tooltip-box">
                          <div className="tooltip-cover">
                            <h3>STEP 1</h3>
                            <p>Start by adding some text</p>
                          </div>
                        </div>
                      </div>,
                      content:
                        <div className="v-import-image">
                          <div className="v-image-list">
                            {this.state.images.map((item,index) => (
                              <div className="v-image-row" key={index.toString()}>
                                <div className="v-label">
									                <p><span>({this.state.imageSizes[index][0]}x{this.state.imageSizes[index][1]}px)</span>  </p>
								                </div>
                                <div className="v-img">
                                  {this.state.loadingImages[index] ? <Loader /> :
                                    (this.state.images[index]
                                      ? <img src={this.state.images[index]}
                                        alt={`Image ${index + 1}`}
                                        style={{ width: 80 }} />
                                      : <div>Select Image</div>)
                                  }
                                </div>
                                <div className="v-desc">
                                  <p>Background {index + 1}</p>

                                  <Icon onClick={() => this.handleImageChange(index)}>edit</Icon>
                                </div>
                              </div>
                            ))}
                          </div>
                        </div>
                    },

                    {
                      title: <div key="audio" className={this.state.listClassnames.audio}
                        onMouseEnter={() => this.handleMouseEnter("audio")}
                        onMouseLeave={this.handleMouseLeave.bind(this)}>
                        <p><Icon>music_note</Icon>Audio</p>
                        <div className="tooltip-box">
                          <div className="tooltip-cover">
                            <h3>STEP 1</h3>
                            <p>Start by adding some text</p>
                          </div>
                        </div>
                      </div>,
                      extra: <ColorButton variant="contained" color="primary" size="small" className="extraButton">
                        Add <AddIcon />
                      </ColorButton>,
                      content:
                        <div className="v-import-audio">
                          {this.state.sounds.map((item,index) => (
                            
                            <div key={index.toString()}>
                              <div className="v-audio-name-block">
                                <p>
                                  Track name - 02 - bg sound
                                  <Icon onClick={() => this.handleSoundChange(index)}>edit</Icon>
                                  <Icon onClick={() => this.handleArrayChange('sounds', index, null)}>delete</Icon>
                                </p>
                              </div>
                              <div className="v-ctm-content">
                                <div className="v-audio-bar">
                                  {this.state.loadingSounds[index] ? <Loader /> :
                                    (this.state.sounds[index]
                                      ? <WaveForm index={index} url={this.state.sounds[index]} />
                                      : <div>Select Audio</div>)
                                  }
                                </div>
                                {/* <div className="v-bottom-cover">
                                  <Icon>volume_up</Icon>
                                  <div className="v-controll-path"><span className="v-path-dot"/></div>                                  
                                  <p>13%</p>  
                                </div> */}
                              </div>
                            </div>
                          ))}
                        </div>
                    }
                  ]}
                />
              </div>
              <div className="v-video-preview-block">
                <ReactPlayer url={this.state.template} width={600} height={300} controls={true}/>
              </div>
              <div className="v-tips-block last-cover">
                <div className="top-link">
                  <a href="#" title="Tip" className="link"><span className="img-cover">
                    <WbIncandescentIcon /></span>Tip: Press space bar to play
                    <span className="img-cover">
                      <CloseIcon />
                    </span>
                  </a>
                  <a href="#" title="Next Tip" className="link next-btn">
                    Next Tip
                    <span className="img-cover">
                      <ArrowForwardIcon />
                    </span>
                  </a>
                </div>
                <div className="">
                  <a href="#" title="Hide timeline" className="link">
                    Hide timeline
                    <span className="img-cover">
                      <VisibilityOffIcon />
                    </span>
                  </a>
                </div>
              </div>
              <div className="timeline-box">
                <div className="v-time-block">
                  <div className="v-time-label">
                    <p>Time</p>
                  </div>
                  <div className="v-time-bar">
                    <p className="v-start-time">01:20</p>
                    <p className="v-end-time">08:30</p>
                  </div>
                </div>
                <div className="v-action-block">
                  <div className="v-text-label">
                    <p>Text</p>
                  </div>
                  <div className="v-action-bar">
                    {this.state.text.map((text, index) => (
                      <div className="v-text-box" key={index.toString()}>
                        <div className="v-edit-popup">
                          <p>edit<span><img src={icon15} alt="" /></span></p>
                        </div>
                        <h6>TEXT {index + 1}<span className="v-sub-icon"><img src={icon9} alt="" /></span></h6>
                        <p>{text}</p>
                      </div>
                    ))}
                  </div>
                </div>
                <div className="v-action-block">
                  <div className="v-text-label">
                    <p>Image</p>
                  </div>
                  <div className="v-action-bar">
                    {this.state.images.map((image, index) => (
                      <div className="v-img-box" key={index}>
                        <div className="v-edit-popup">
                          <p>edit<span><img src={icon15} alt="" /></span></p>
                        </div>
                        <img src={image} alt="" style={{ width: 80 }} />
                      </div>
                    ))}
                  </div>
                </div>
                <div className="v-action-block">
                  <div className="v-text-label">
                    <p>Video</p>
                  </div>
                  <div className="v-action-bar">
                    <div className="v-video-box">

                      <CustomDropdown
                        dropup
                        buttonColor={["defaultNoBackground"]}
                        buttonText={
                          <div className="v-edit-popup">
                            <p>edit<span><img src={icon15} alt="" /></span></p>
                          </div>
                        }
                        dropdownList={[
                          <div className="pro-cover">
                            <div className="v-action-text">
                              <p className="marginBottomZ">Replace</p>
                            </div>
                            <div className="v-action-icon">
                              <Icon>edit</Icon>
                            </div>
                          </div>,
                          <div className="pro-cover">
                            <div className="v-action-text">
                              <p className="marginBottomZ">Delete</p>
                            </div>
                            <div className="v-action-icon">
                              <Icon>delete</Icon>
                            </div>
                          </div>
                        ]}
                      />

                      <img src={imgInput} alt="" />
                    </div>
                    <div className="v-video-box">

                      <CustomDropdown
                        dropup
                        buttonColor={["defaultNoBackground"]}
                        buttonText={
                          <div className="v-edit-popup">
                            <p>edit<span><img src={icon15} alt="" /></span></p>
                          </div>
                        }
                        dropdownList={[
                          <div className="pro-cover">
                            <div className="v-action-text">
                              <p className="marginBottomZ">Replace</p>
                            </div>
                            <div className="v-action-icon">
                              <Icon>edit</Icon>
                            </div>
                          </div>,
                          <div className="pro-cover">
                            <div className="v-action-text">
                              <p className="marginBottomZ">Delete</p>
                            </div>
                            <div className="v-action-icon">
                              <Icon>delete</Icon>
                            </div>
                          </div>
                        ]}
                      />

                      <img src={imgInput} alt="" />
                    </div>
                    <div className="v-video-box">

                      <CustomDropdown
                        dropup
                        buttonColor={["defaultNoBackground"]}
                        buttonText={
                          <div className="v-edit-popup">
                            <p>edit<span><img src={icon15} alt="" /></span></p>
                          </div>
                        }
                        dropdownList={[
                          <div className="pro-cover">
                            <div className="v-action-text">
                              <p className="marginBottomZ">Replace</p>
                            </div>
                            <div className="v-action-icon">
                              <Icon>edit</Icon>
                            </div>
                          </div>,
                          <div className="pro-cover">
                            <div className="v-action-text">
                              <p className="marginBottomZ">Delete</p>
                            </div>
                            <div className="v-action-icon">
                              <Icon>delete</Icon>
                            </div>
                          </div>
                        ]}
                      />

                      <img src={imgInput} alt="" />
                    </div>
                    <div className="v-video-box">

                      <CustomDropdown
                        dropup
                        buttonColor={["defaultNoBackground"]}
                        buttonText={
                          <div className="v-edit-popup">
                            <p>edit<span><img src={icon15} alt="" /></span></p>
                          </div>
                        }
                        dropdownList={[
                          <div className="pro-cover">
                            <div className="v-action-text">
                              <p className="marginBottomZ">Replace</p>
                            </div>
                            <div className="v-action-icon">
                              <Icon>edit</Icon>
                            </div>
                          </div>,
                          <div className="pro-cover">
                            <div className="v-action-text">
                              <p className="marginBottomZ">Delete</p>
                            </div>
                            <div className="v-action-icon">
                              <Icon>delete</Icon>
                            </div>
                          </div>
                        ]}
                      />

                      <img src={imgInput} alt="" />
                    </div>
                    <div className="v-video-box">

                      <CustomDropdown
                        dropup
                        buttonColor={["defaultNoBackground"]}
                        buttonText={
                          <div className="v-edit-popup">
                            <p>edit<span><img src={icon15} alt="" /></span></p>
                          </div>
                        }
                        dropdownList={[
                          <div className="pro-cover">
                            <div className="v-action-text">
                              <p className="marginBottomZ">Replace</p>
                            </div>
                            <div className="v-action-icon">
                              <Icon>edit</Icon>
                            </div>
                          </div>,
                          <div className="pro-cover">
                            <div className="v-action-text">
                              <p className="marginBottomZ">Delete</p>
                            </div>
                            <div className="v-action-icon">
                              <Icon>delete</Icon>
                            </div>
                          </div>
                        ]}
                      />

                      <img src={imgInput} alt="" />
                    </div>
                  </div>
                </div>
                <div className="v-action-block">
                  <div className="v-text-label">
                    <p>Audio</p>
                  </div>
                  <div className="v-action-bar">
                    <div className="v-audio-box">
                      <CustomDropdown
                        dropup
                        buttonColor={["defaultNoBackground"]}
                        buttonText={
                          <div className="v-edit-popup">
                            <p>edit<span><img src={icon15} alt="" /></span></p>
                          </div>
                        }
                        dropdownList={[
                          <div className="pro-cover">
                            <div className="v-action-text">
                              <p className="marginBottomZ">Replace</p>
                            </div>
                            <div className="v-action-icon">
                              <Icon>edit</Icon>
                            </div>
                          </div>,
                          <div className="pro-cover">
                            <div className="v-action-text">
                              <p className="marginBottomZ">Delete</p>
                            </div>
                            <div className="v-action-icon">
                              <Icon>delete</Icon>
                            </div>
                          </div>,
                          <div className="pro-cover">
                            <div className="v-ctm-content">
                              <p className="v-link-title">Volume</p>
                              <div className="v-bottom-cover">
                                <div className="v-controll-path"><span className="v-path-dot"></span></div>
                                <p>13%</p>
                              </div>
                            </div>
                          </div>
                        ]}
                      />
                      <img src={videoPath} alt="" />
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </Card>
        </React.Fragment>
      </GridContainer>
    );
  }
}

AdCreator.propTypes = {
  classes: PropTypes.object
};

const mapStateToProps = ({ router: { location: { state: routerState } }, adCreator: { postVideoResult }, adCreator: { getVideoResult }, adCreator: { postSessionResult } }) => ({ routerState, postVideoResult, getVideoResult,postSessionResult });



const mapDispatchToProps = dispatch => ({
  getAdVideo: () => dispatch(getAdVideo()),
  postVideo: changes => dispatch(postVideo(changes)),
  postSession: changes => dispatch(postSession(changes)),
  postNotification: changes => dispatch(postNotification(changes)),
  getVideo: () => dispatch(getVideo()),
  getNotification: () => dispatch(getNotification()),
  uploadAdVideoImages: image => dispatch(uploadAdVideoImage(image))
});

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(regularFormsStyle)(AdCreator));
