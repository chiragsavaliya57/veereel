import React from 'react';
import cx from 'classnames';
import './style.css';

function Loader({ className }) {
  return <div className={cx("spinner", className)}>
    <div className="rect1" />
    <div className="rect2" />
    <div className="rect3" />
    <div className="rect4" />
    <div className="rect5" />
  </div>;
}

export default React.memo(Loader);