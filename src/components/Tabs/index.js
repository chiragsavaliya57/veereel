import React from 'react';

import CustomTabs from '../../assets/jss/material-dashboard-pro-react/components/CustomTabs/CustomTabs';
import './style.css';


function Tabs({tabData, isActive}) {
  return (
    <div className="tab-container">
      <CustomTabs
        tabs={tabData}
        isActive={isActive}
      />
    </div>
  );
}

// tabData = [
//   {
//     tabName: "",
//     tabContent: ""
//   }
// ];

export default Tabs;