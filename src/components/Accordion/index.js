import React, { useEffect, useState } from 'react';

import withStyles from '@material-ui/core/styles/withStyles';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';

import Tooltips from '../../components/Tooltip';
import cx from 'classnames';
import './style.css';


import ExpandMore from '@material-ui/icons/ExpandMore';
import accordionStyle from '../../assets/jss/material-dashboard-pro-react/components/accordionStyle.jsx';

function Accordion({classes, data, active}) {
  const [content, setContent] = useState([]);
  const [curActive, setCurActive] = useState(-1);

  useEffect(() => {
    const newContent = [];
    data.forEach((item) => {
      newContent.push({
        title: item.title,
        extraActiveTitle: item.extraActiveTitle,
        toolTip: item.toolTip,
        content: item.content.map((content) => <div className="accordion-content">{content}</div>),
      });
    });
    if (active) setCurActive(active);
    setContent(newContent);
  }, []);

  const handleChange = panel => (event, expanded) => {
    if (event.target.localName === 'button') return;

    setCurActive(expanded ? panel : -1);
  };

  return (
    <div className={cx(classes.root, 'accordion-container')}>
      {content.map((item, key) => {
        return (
          <ExpansionPanel
            expanded={curActive === key}
            onChange={handleChange(key)}
            key={key}
            classes={{
              root: cx(curActive === -1 || curActive === key ? classes.expansionPanel : 'd-n', 'accordion-tab-title'),
              expanded: cx(classes.expansionPanelExpanded, 'accordion-tab-active'),
            }}
          >
            <ExpansionPanelSummary
              expandIcon={<ExpandMore/>}
              classes={{
                root: classes.expansionPanelSummary,
                expanded: 'active-accordion-tab',
                content: classes.expansionPanelSummaryContent,
                expandIcon: classes.expansionPanelSummaryExpandIcon,
              }}
            >
              {item.toolTip ?
                <div className="active-accordion-title-container">
                  <h4 className={classes.title}>
                    <Tooltips
                      content={item.toolTip}
                      placement={'right'}
                    >{item.title}
                    </Tooltips>
                  </h4>
                  {item.extraActiveTitle && (curActive === key) && item.extraActiveTitle}
                </div> :
                <div className="active-accordion-title-container">
                  <h4 className={classes.title}>{item.title}</h4>
                  {item.extraActiveTitle && (curActive === key) && item.extraActiveTitle}
                </div>}

            </ExpansionPanelSummary>
            <ExpansionPanelDetails className={classes.expansionPanelDetails}>
              {item.content}
            </ExpansionPanelDetails>
          </ExpansionPanel>
        );
      })}
    </div>
  );
}

export default withStyles(accordionStyle)(Accordion);
