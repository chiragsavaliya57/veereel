import React, {useState} from "react";
import {useDispatch, useSelector} from 'react-redux';
import { auth, registration } from '../../reducer/auth/actions';

import Button from "../../components/Button"
import Input from "../../components/Input"
import Loader from "../../components/Loader"

import "./style.css";

function RegistrationMainPage() {
  const dispatch = useDispatch();
  const {error, isLoading, payload} = useSelector(state => state.auth.registration);
  const [isValid, setIsValid] = useState({
    email: true,
    password_1: true,
    password_2: true,
    password_same: true,
  });
  const [userInfo,setUserInfo] = useState({
    email: '',
    password_1: '',
    password_2: ''
  });

  const handleChangeInputs = (type, value) => {
    setUserInfo({...userInfo, [type]: value})
  };

  const handleSubmit = () => {
    const checkValid = {
      email: /^\w+\@\w+\.\w{2,4}$/.test(userInfo.email),
      password_1: /\w{3,}/.test(userInfo.password_1),
      password_2: /\w{3,}/.test(userInfo.password_2),
      password_same: userInfo.password_1 ===  userInfo.password_2
    };

    setIsValid(checkValid);

    if (checkValid.email && checkValid.password_1 && checkValid.password_2 && checkValid.password_same) {
      dispatch(registration(userInfo));
    }
  };

  return (
    <div className="registration-main-page">
      <h1 className="registration-main-page-header">Registration</h1>
      <p className="registration-main-page-error-container">{error && error.join(", ")}</p>
      <div className="registration-main-page-input-container">
        <Input
          type="text"
          value={userInfo.name}
          onChange={(e) => handleChangeInputs('name', e)}
          labelText={"Name"}
          className="white-input"
        />
        <Input
          type="text"
          value={userInfo.email}
          onChange={(e) => handleChangeInputs('email', e)}
          labelText={"Email"}
          className="white-input"
          error={!isValid.email}
          helperText={!isValid.email && "Email is not valid"}
        />
        <Input
          type="password"
          value={userInfo.password_1}
          onChange={(e) => handleChangeInputs('password_1', e)}
          labelText={"Password"}
          className="white-input"
          error={!isValid.password_1 || !isValid.password_same}
          helperText={(!isValid.password_1 && "Password is required") || (!isValid.password_same && "Passwords should be the same")}
        />
        <Input
          type="password"
          value={userInfo.password_2}
          onChange={(e) => handleChangeInputs('password_2', e)}
          labelText={"Repeat Password"}
          className="white-input"
          error={!isValid.password_2 || !isValid.password_same}
          helperText={(!isValid.password_2 && "Password is required") || (!isValid.password_same && "Passwords should be the same")}
        />
      </div>
      <div className="registration-main-page-btn-container">
        <Button onClick={handleSubmit} disabled={isLoading}>SIGN UP</Button>
      </div>
      {
        isLoading && <div className="login-main-page-loading" ><Loader className="loader-main-page" /></div>
      }
    </div>
  );
}

export default  React.memo(RegistrationMainPage);
