import {
	GET_AD_VIDEO,
	GET_AD_VIDEO_SUCCESS,
	GET_AD_VIDEO_FAILED,

	GET_AD_VIDEO_TASKS,
	GET_AD_VIDEO_TASKS_SUCCESS,
	GET_AD_VIDEO_TASKS_FAILED,

	UPLOAD_AD_VIDEO_IMAGE,
	UPLOAD_AD_VIDEO_IMAGE_SUCCESS,
	UPLOAD_AD_VIDEO_IMAGE_FAILED,

	UPLOAD_AD_VIDEO_SOUND,
	UPLOAD_AD_VIDEO_SOUND_SUCCESS,
	UPLOAD_AD_VIDEO_SOUND_FAILED,

	POST_VIDEO,
	POST_VIDEO_SUCCESS,
	POST_VIDEO_FAILED,

	GET_VIDEO,
	GET_VIDEO_SUCCESS,
	GET_VIDEO_FAILED,

	POST_SESSION,
  	POST_SESSION_SUCCESS,
  	POST_SESSION_FAILED,
  
  	GET_SESSION2,
  	GET_SESSION_SUCCESS,
  	GET_SESSION_FAILED,
  
	GET_SCRIPT_SUGGESTION,
	GET_SCRIPT_SUGGESTION_SUCCESS,
	GET_SCRIPT_SUGGESTION_FAILED,
	POST_NOTIFICATION,
	POST_NOTIFICATION_SUCCESS,
	POST_NOTIFICATION_FAILED,
	GET_NOTIFICATION,
	GET_NOTIFICATION_SUCCESS,
	GET_NOTIFICATION_FAILED,
	DELETE_NOTIFICATION,
	DELETE_NOTIFICATION_SUCCESS,
	DELETE_NOTIFICATION_FAILED
} from './constants';

export const getAdVideo = () => (dispatch, getState, {AdCreatorService}) => {

	dispatch({
		type: GET_AD_VIDEO
	});

	return AdCreatorService.getVideos().then(payload => {
    dispatch({
      type: GET_AD_VIDEO_SUCCESS,
      payload
    })
  }).catch(payload => {
    dispatch({
      type: GET_AD_VIDEO_FAILED,
      payload
    })
  })
};

export const getAdVideoTasks = () => (dispatch, getState, {AdCreatorService}) => {
  dispatch({
    type: GET_AD_VIDEO_TASKS
  });

  return AdCreatorService.getTasks().then(payload => {
    dispatch({
      type: GET_AD_VIDEO_TASKS_SUCCESS,
      payload
    })
  }).catch(payload => {
    dispatch({
      type: GET_AD_VIDEO_TASKS_FAILED,
      payload
    })
  })
};

export const uploadAdVideoImage = image => (dispatch, getState, {AdCreatorService}) => {
  dispatch({
    type: UPLOAD_AD_VIDEO_IMAGE
  });

  return AdCreatorService.uploadImage(image).then(payload => {
    dispatch({
      type: UPLOAD_AD_VIDEO_IMAGE_SUCCESS,
      payload
    })
  }).catch(payload => {
    dispatch({
      type: UPLOAD_AD_VIDEO_IMAGE_FAILED,
      payload
    })
  })
};

export const uploadAdVideoSound = sound => (dispatch, getState, {AdCreatorService}) => {
  dispatch({
    type: UPLOAD_AD_VIDEO_SOUND
  });

  return AdCreatorService.uploadSound(sound).then(payload => {
    dispatch({
      type: UPLOAD_AD_VIDEO_SOUND_SUCCESS,
      payload
    })
  }).catch(payload => {
    dispatch({
      type: UPLOAD_AD_VIDEO_SOUND_FAILED,
      payload
    })
  })
};

export const postVideo = changes => (dispatch, getState, {AdCreatorService}) => {
  dispatch({
    type: POST_VIDEO
  });

  return AdCreatorService.postVideo(changes).then(payload => {
    dispatch({
      type: POST_VIDEO_SUCCESS,
      payload
    })
  }).catch(payload => {
    dispatch({
      type: POST_VIDEO_FAILED,
      payload
    })
  })
};

export const getVideo = () => (dispatch, getState, {AdCreatorService}) => {
  dispatch({
    type: GET_VIDEO
  });

  return AdCreatorService.getVideo().then(payload => {
    dispatch({
      type: GET_VIDEO_SUCCESS,
      payload
    })
  }).catch(payload => {
    dispatch({
      type: GET_VIDEO_FAILED,
      payload
    })
  })
};

export const postSession = changes => (dispatch, getState, {AdCreatorService}) => {
  dispatch({
    type: POST_SESSION
  });

  return AdCreatorService.postSession(changes).then(payload => {
    dispatch({
      type: POST_SESSION_SUCCESS,
      payload
    })
  }).catch(payload => {
    dispatch({
      type: POST_SESSION_FAILED,
      payload
    })
  })
};

export const getSession = () => (dispatch, getState, {AdCreatorService}) => {
  dispatch({
    type: GET_SESSION2
  });

  return AdCreatorService.getSession().then(payload => {
    dispatch({
      type: GET_SESSION_SUCCESS,
      payload
    })
  }).catch(payload => {
    dispatch({
      type: GET_SESSION_FAILED,
      payload
    })
  })
};
export const getScriptSuggestion = () => (dispatch, getState, {AdCreatorService}) => {
  dispatch({
    type: GET_SCRIPT_SUGGESTION
  });

  return AdCreatorService.getScriptSuggestion().then(payload => {
    dispatch({
      type: GET_SCRIPT_SUGGESTION_SUCCESS,
      payload
    })
  }).catch(payload => {
    dispatch({
      type: GET_SCRIPT_SUGGESTION_FAILED,
      payload
    })
  })
};

export const postNotification = (changes) => (dispatch, getState, { AdCreatorService }) => {
	dispatch({
		type: POST_NOTIFICATION
	});

	return AdCreatorService.postNotification(changes)
		.then((payload) => {
			dispatch({
				type: POST_NOTIFICATION_SUCCESS,
				payload
			});
		})
		.catch((payload) => {
			dispatch({
				type: POST_NOTIFICATION_FAILED,
				payload
			});
		});
};

export const getNotification = () => (dispatch, getState, { AdCreatorService }) => {
	dispatch({
		type: GET_NOTIFICATION
	});

	return AdCreatorService.getNotification()
		.then((payload) => {
			dispatch({
				type: GET_NOTIFICATION_SUCCESS,
				payload
			});
		})
		.catch((payload) => {
			dispatch({
				type: GET_NOTIFICATION_FAILED,
				payload
			});
		});
};

export const deleteNotification = (data) => (dispatch, getState, { AdCreatorService }) => {
	dispatch({
		type: DELETE_NOTIFICATION
	});

	return AdCreatorService.deleteNotification(data)
		.then((payload) => {
			dispatch({
				type: DELETE_NOTIFICATION_SUCCESS,
				payload
			});
		})
		.catch((payload) => {
			dispatch({
				type: DELETE_NOTIFICATION_FAILED,
				payload
			});
		});
};
