import React from 'react';
import {Redirect, Route} from 'react-router-dom';
import {connect} from 'react-redux';

const ProtectedRoute = ({component: Component, allow, ...rest}) =>
  <Route {...rest} render={props => allow ? <Component {...props}/> : <Redirect to="/main/login-page"/>}/>;

export default connect(({auth: {login: {payload}}}) => ({
  allow: !!(payload && payload.email)
}))(ProtectedRoute);
