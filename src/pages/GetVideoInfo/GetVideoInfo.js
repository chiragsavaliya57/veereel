import React, {useState, useMemo, useEffect} from "react";
import {useDispatch, useSelector} from 'react-redux'
import {getVideo} from '../../reducer/videoInfo/actions';

function  GetVideoInfo () {
  const dispatch = useDispatch();
  const [text, setText] = useState("");

  const handleChangeText = (event) => {
   setText(event.target.value);
  }
  //
 const handleOnClick = (event) => {
    dispatch(getVideo(text));
  };


  return (<div>
		GetVideo
    <div>
			<input value={text} onChange={handleChangeText} />
		</div>
    <div>
      <button onClick={handleOnClick}>Load</button>
    </div>
    {/*<div>Link: <a href={this.state.link}>{this.state.link}</a></div>*/}
    {/*<div>Title: {this.state.title}</div>*/}
    {/*<div>Description: {this.state.description}</div>*/}
    {/*<div>Category: {this.state.category}</div>*/}
    {/*<div>Duration: {this.state.duration}</div>*/}
    {/*<div>Tags: {this.state.tags}</div>*/}
    {/*<div>Thumbnail: <img src={this.state.thumbnailSrc}/></div>*/}
  </div>);
}



export default GetVideoInfo;

/*
<source src="video.mp4" type="video/mp4"><!-- MP4 для Safari, IE9, iPhone, iPad, Android, и Windows Phone 7 -->
		  <source src="video.webm" type="video/webm"><!-- WebM/VP8 для Firefox4, Opera, и Chrome -->
		  <source src="video.ogv" type="video/ogg"><!-- Ogg/Vorbis для старых версий браузеров Firefox и Opera -->
		  <object data="video.swf" type="application/x-shockwave-flash"><!-- добавляем видеоконтент для устаревших браузеров, в которых нет поддержки элемента video -->
			<param name="movie" value="video.swf">
		  </object>*/