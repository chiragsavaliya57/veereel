import React, {Component} from 'react';

import CloseIcon from '@material-ui/icons/Close';

import MuiDialogTitle from "@material-ui/core/DialogTitle/DialogTitle";
import TextRotationNone from "@material-ui/core/SvgIcon/SvgIcon";
import DialogContent from "@material-ui/core/DialogContent";
import Dialog from "@material-ui/core/Dialog";

import GridContainer from "components/Grid/GridContainer.jsx";
import GridItem from "components/Grid/GridItem.jsx";
import NavPills from 'components/NavPills/NavPills.jsx';



class SuggestionsModal extends Component {
  

  handlerSelectScript(item,item1){
    console.log(item,item1)
    //this.props.onClose = {item,item1};
  }

  render() {
    const {open, suggestionScript, categories, onClose} = this.props;
    
    const tabContentItem = (
      <div className="cover-div card-modal">
        <p><span>01</span>Hey, Check this product out!</p>
        <p><span>02</span>Get a 30% discount today.</p>
        <p><span>03</span>Free delivery in Europe.</p>
        <p><span>04</span>Only few products left.</p>
        <p><span>05</span>Hurry, don't miss out.</p>
      </div>
    );

    const tabContentItemSpecial = (
      <div className="cover-div">
        <div className="video-cover" id="cover-for-video-script">
          <a className="card-modal" href="#" title="">
            <p><span>01</span>Hey, Check this product out!</p>
            <p><span>02</span>Get a 30% discount today.</p>
            <p><span>03</span>Free delivery in Europe.</p>
            <p><span>04</span>Only few products left.</p>
            <p><span>05</span>Hurry, don't miss out.</p>
          </a>
        </div>
        <div className="video-content" id="cover-for-video-content">
          <div className="btn-content">
            <a href="#" title="Select" className="btn">Select</a>
          </div>
        </div>
      </div>

    );
    const tabContent = (
      <GridContainer>
        <GridItem xs={12} sm={12} md={12} style={{marginBottom: 30}}>
          <h5>Note:</h5>
          Completely synergize resource taxing relationships
          via premier niche markets. Professionally cultivate
          one-to-one customer service with robust ideas.{" "}
        </GridItem>
        <GridItem xs={12} sm={12} md={6}>
          {tabContentItemSpecial}
        </GridItem>
        <GridItem xs={12} sm={12} md={6}>
          {tabContentItem}
        </GridItem>
        <GridItem xs={12} sm={12} md={6}>
          {tabContentItem}
        </GridItem>
        <GridItem xs={12} sm={12} md={6}>
          {tabContentItem}
        </GridItem>
      </GridContainer>
    );

    let tabsArr = new Array();
    this.props.categories.map((item, key) =>{
      let eleArr = new Array();
      {this.props.suggestionScript.map((rec, key) =>{
        if(rec.categoryId == item.id){
          eleArr.push(rec)
        }
      })}
      let tabsCtnt = 
        (<GridContainer>
            <GridItem xs={12} sm={12} md={12} style={{marginBottom: 30}}>
              <h5>Note:</h5>
              Completely synergize resource taxing relationships
              via premier niche markets. Professionally cultivate
              one-to-one customer service with robust ideas.{" "}
            </GridItem> 
            {eleArr.map((item1, key) =>(
              <GridItem xs={12} sm={12} md={6} key={key}>
              <div className="cover-div">
                <div className="video-cover" id="cover-for-video-script">
                  <a className="card-modal" href="#" title="">
                    {item1.text.map((lineText, key) => (
                      <p key={key+1}><span>{key+1}</span>{lineText}</p>
                    ))}
                  </a>
                </div>
                <div className="video-content" id="cover-for-video-content">
                  <div className="btn-content">
                    <a href="javascript:;" title="Select" className="btn" onClick={() => onClose(item1)}>Select</a>
                  </div>
                </div>
              </div>
            </GridItem>
            ))}
        </GridContainer>) 
      tabsArr.push({tabButton:item.Name,tabContent:tabsCtnt})}
    );

    return (
      <Dialog
        open={open}
        onClose={onClose}
        aria-labelledby="form-dialog-title"
        maxWidth="lg"
        className="suggestions-modal"
      >
        <MuiDialogTitle id="form-dialog-title">
          <TextRotationNone/>Choose Script
          <span onClick={onClose}><CloseIcon/></span>
        </MuiDialogTitle>
        <DialogContent>
          <NavPills
            color="default"
            horizontal={{
              tabsGrid: {xs: 12, sm: 12, md: 4},
              contentGrid: {xs: 12, sm: 12, md: 8}
            }}

            tabs={tabsArr}
          />
        </DialogContent>
      </Dialog>
    );
  }
}

export default SuggestionsModal; 
