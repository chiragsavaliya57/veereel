import React, { useState } from 'react';

import { useDispatch, useSelector } from 'react-redux';
import { auth, logOut } from '../../reducer/auth/actions';

import Button from '../../components/Button';
import Input from '../../components/Input';
import SubHeaderEditable from '../../components/SubHeaderEditable';

import './style.css';

function Login() {
  const dispatch = useDispatch();
  const [userInfo, setUserInfo] = useState({});
  const [editalbeTitle, setEditalbeTitle] = useState('Login');
  const {error, isLoading} = useSelector(state => state.auth.login);

  const handleChangeEmail = (event) => {
    setUserInfo({...userInfo, email: event});
  };

  const handleChangePassword = (event) => {
    setUserInfo({...userInfo, password: event});
  };

  const handleSubmit = () => {
    dispatch(auth(userInfo));
  };

  const handleSubmitlogOut = () => {
    dispatch(logOut());
  };

  const updateTitle = (value) => {
    setEditalbeTitle(value);
  };

// TODO replace to default error view
// TODO replace to default loader
  return (
    <div className="login-container">
      <SubHeaderEditable
        handleSave={updateTitle}
        initialValue={editalbeTitle}
        changeBlock={props => (
          <Input {...props}
                 className="editable_input"
                 onChange={m => props.handleChange(m || null)}/>)}>
        <h1>{editalbeTitle}</h1>
      </SubHeaderEditable>
      <div>
        <Input
          type="text"
          value={userInfo.email}
          onChange={handleChangeEmail}
          placeholder={'Enter email'}
          labelText={'Email'}
        />
      </div>
      <div>
        <Input
          type="password"
          value={userInfo.password}
          onChange={handleChangePassword}
          placeholder={'Enter password'}
          labelText={'Password'}
        />
      </div>
      <div className="login-button-container">
        <Button onClick={handleSubmit}>Login</Button>
        <Button onClick={handleSubmitlogOut}>Log out</Button>
      </div>
      {error && <h2>{error}</h2>}
      {isLoading && <i>Loading...</i>}
    </div>
  );
}

export default Login;