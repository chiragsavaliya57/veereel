import React, { useState, useEffect } from 'react';
import ImageUpload from '../../assets/jss/material-dashboard-pro-react/components/CustomUpload/ImageUpload.jsx';
import defaultAvatar from "../../assets/img/placeholder.jpg";

import './style.css';


function UpdateImage({value, onSave, defautImage}) {

  return (
    <div className="update-image-container">
      <ImageUpload
        avatar
        changeButtonProps={{className: 'update-image-btn'}}
        defaultAvatar={defautImage || defaultAvatar}
        onSave={(e) => onSave(e)}
        addButtonProps={{className: 'update-image-btn'}}
      />
    </div>
  );
}


export default UpdateImage;