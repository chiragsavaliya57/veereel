import React from 'react';
import axios from 'axios';

class Register extends React.Component {
	
  constructor(props) {
    super(props);
    this.state = {status: '', loginLink:document.location.origin+"/login"};
	
	this.handleChangeEmail = this.handleChangeEmail.bind(this);
    this.handleChangePassword1 = this.handleChangePassword1.bind(this);
	this.handleChangePassword2 = this.handleChangePassword2.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChangeEmail(event) {
    this.setState({email: event.target.value});
  }
  handleChangePassword1(event) {
    this.setState({password_1: event.target.value});
  }
  handleChangePassword2(event) {
    this.setState({password_2: event.target.value});
  }

  handleSubmit(event) {
    //alert('A name was submitted: '+ this.state.email + this.state.password);
	var parObj = this;
	axios({
	  method: 'post',
	  url: document.location.origin+"/api/register_server.php",
	  data: {
		email: this.state.email,
		password_1: this.state.password_1,
		password_2: this.state.password_2
	  }
	}).then(function (response) {
		console.log(response);
		if(response.data.responseFlag) {
			parObj.setState({status: "Registration is successful!"});
		} else {
			parObj.setState({status: "ERRORS: "+response.data.errors.join(', ')});
		}
	  });	
    event.preventDefault();
  }

  render() {
    return (
      <form onSubmit={this.handleSubmit}>
		<h>Registration</h>
        <div>
          Email:
          <input type="text" value={this.state.email} onChange={this.handleChangeEmail} />
        </div>
		<div>
          Password:
          <input type="password" value={this.state.password_1} onChange={this.handleChangePassword1} />
        </div>
		<div>
          Password again:
          <input type="password" value={this.state.password_2} onChange={this.handleChangePassword2} />
        </div>
        <input type="submit" value="Register" />
		<a href={this.state.loginLink}>Login</a>
		<div>
          {this.state.status}
        </div>
      </form>
    );
  }

}

export default Register;