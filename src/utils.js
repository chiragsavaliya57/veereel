export function isProtected(path) {
  return path !== '/login-page' && path !== '/register-page';
}

function selectFile(options, cb) {
  let input = Object.assign(document.createElement('input'), options);
  input.type = 'file';
  input.multiple = false;
  input.onchange = function () {
    cb(this.files[0]);
  };
  input.click();
}

export function inputImage(cb) {
  selectFile({accept: 'image/jpg, image/png'}, cb);
}

export function inputSound(cb) {
  selectFile({accept: 'audio/mpeg'}, cb);
}
