import React from 'react';
import axios from 'axios';
import $ from 'jquery';

import {UserInfoContext} from '../../contexts/UserInfoContext.js';

const WithContext = (Component) => {
  return (props) => (
      <UserInfoContext.Consumer>
           {value =>  <Component {...props} value={value} />}
      </UserInfoContext.Consumer>
  )
}

class KeywordIdeas extends React.Component {
	
  constructor(props) {
    super(props);
    this.state = {text:"", answer:'', neededNumber:10};
	this.handleChangeText = this.handleChangeText.bind(this);
	this.handleChangeText2 = this.handleChangeText2.bind(this);
	this.handleOnClick = this.handleOnClick.bind(this);
	this.getKeyword_search = this.getKeyword_search.bind(this);
  }
  
  getKeyword_search(m,query,max,cmax){
	  
	  
	  
	  var parObj =this;

  	var arr = ["","b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "x", "y", "z", "0", "1", "2", "3", "4", "5", "6", "7", "8", "9"];

      if(m == 0)
      {
        var yurl = "https://suggestqueries.google.com/complete/search?hl=en&ds=yt&client=youtube&json=t&cp=1&q="+query+"&format=5&alt=json&callback=?";
      }else {
        var yurl = "https://suggestqueries.google.com/complete/search?hl=en&ds=yt&client=youtube&json=t&cp=1&q="+query+"+"+arr[m]+"&format=5&alt=json&callback=?";
      }
	  $.ajax({
        url: yurl,
        dataType: 'json',
        async :true,
        success: function(data, textStatus, request) {
          //alert(data);
          m++;
          var str = data.toString();
          var append = str.split(",");
          //alert(append);
          var len = append.length;
          //alldata += append;
          var arr = [];
          for(var i=1;i<len-2;i++)
          {
            arr.push(append[i]);
            cmax++;
            //console.log(cmax +" maxsize"+max);
            if(cmax==max){
              break;
            }
          }
          var arlen = arr.length;
          var q=0;
          for(var j=0;j<arlen;j++)
          {
            q=q+1;
            if(q >10){ q=1; }else{ q = q; }
            var no = parseInt(j+1);
            if(arr[j] != '')
            {
			  var delimeter = ', ';
			  if(parObj.state.answer.length<1) {
				  delimeter = '';
			  }
              parObj.setState(prevState => ({answer: prevState.answer + 
			  delimeter+arr[j]}));
            }
          }
          if(cmax == max || m >= 35 ){
            return true;
          }else{
            parObj.getKeyword_search(m,query,max,cmax);
          }
          //alert(arlen);
		 
		}
	  });
  }
  getKeyword_searchWrap(m,query,max,cmax){
	  var parObj = this;
	  axios
		.get(document.location.origin+"/api/getLogin.php")
		.then(({ data }) => {
			if(data.hasOwnProperty('isLoggedIn') && !data.isLoggedIn) {
				this.props.value.logout();
				window.location.replace("/main/login-page");
			} else {
				parObj.getKeyword_search(m,query,max,cmax);
			}
		});
  }
  componentDidMount() {
  }

  componentWillUnmount() {
	  
  }
  handleChangeText(event) {
    this.setState({text: event.target.value});
  }
  handleChangeText2(event) {
    this.setState({neededNumber: event.target.value});
  }
  handleOnClick(event) {
    
    this.getKeyword_searchWrap(0, this.state.text,this.state.neededNumber,0);
  }
  
  
  render() {
    return (<div><div><input value={this.state.text} onChange={this.handleChangeText}></input></div>
		<div>results number: <input value={this.state.neededNumber} onChange={this.handleChangeText2}></input></div>
		<div><button onClick={this.handleOnClick}>Load</button></div>
		<div>{this.state.answer}</div>
		
	</div>);
    
  }

}

export default WithContext(KeywordIdeas);

/*
<source src="video.mp4" type="video/mp4"><!-- MP4 для Safari, IE9, iPhone, iPad, Android, и Windows Phone 7 -->
		  <source src="video.webm" type="video/webm"><!-- WebM/VP8 для Firefox4, Opera, и Chrome -->
		  <source src="video.ogv" type="video/ogg"><!-- Ogg/Vorbis для старых версий браузеров Firefox и Opera -->
		  <object data="video.swf" type="application/x-shockwave-flash"><!-- добавляем видеоконтент для устаревших браузеров, в которых нет поддержки элемента video -->
			<param name="movie" value="video.swf">
		  </object>*/