import React from 'react';
import axios from 'axios';

import {UserInfoContext} from '../../contexts/UserInfoContext.js';

const WithContext = (Component) => {
  return (props) => (
      <UserInfoContext.Consumer>
           {value =>  <Component {...props} value={value} />}
      </UserInfoContext.Consumer>
  )
}

class SearchChannels extends React.Component {
	
  constructor(props) {
    super(props);
    this.state = {text:"", neededNumber:1, mode: "Keyword", modeId:0, answer:{items:[]}};
	this.handleChangeText = this.handleChangeText.bind(this);
	this.handleChangeText2 = this.handleChangeText2.bind(this);
	this.handleOnClick = this.handleOnClick.bind(this);
	this.handleOnClickKeyword = this.handleOnClickKeyword.bind(this);
	this.handleOnClickChannel = this.handleOnClickChannel.bind(this);
	this.handleOnClickRelation = this.handleOnClickRelation.bind(this);
  }
  
  componentDidMount() {
    
  }

  componentWillUnmount() {
	  
  }
  handleChangeText2(event) {
    this.setState({neededNumber: event.target.value});
  }
  handleChangeText(event) {
    this.setState({text: event.target.value});
  }
  handleOnClickKeyword(event) {
	  this.setState({mode: "Keyword", modeId:0});
  }
  handleOnClickChannel(event) {
	  this.setState({mode: "Channel", modeId:1});
  }
  handleOnClickRelation(event) {
	  this.setState({mode: "Relation", modeId:2});
  }
  handleOnClick(event) {
    var parObj = this;
	axios({
	  method: 'post',
	  url: document.location.origin+"/api/searcher/get_channelsearch_list.php",
	  data: {
		seskeyword: parObj.state.text,
		max: parObj.state.neededNumber
	  }
	}).then(({ data }) => {
			//var data2 = JSON.parse(data);
			console.log(data);
			if(data.hasOwnProperty('isLoggedIn') && !data.isLoggedIn) {
				this.props.value.logout();
				window.location.replace("/main/login-page");
			} else {
				if(data.responseFlag) {
					parObj.setState({answer: data.responseResult});
				} else {
					//TODO
				}
			}
		});
		
  }
  
  
  render() {
    return (<div>
		<div><input value={this.state.text} onChange={this.handleChangeText}></input></div>
		<div>results number: <input value={this.state.neededNumber} onChange={this.handleChangeText2}></input></div>
		<div><button onClick={this.handleOnClick}>Load</button></div>
		<div>
		<table>
		<th>Link</th><th>Title</th><th>Description</th>
		{this.state.answer.items.map((item) => 
			<tr><td><a href={"https://www.youtube.com/channel/"+item.id.channelId}>Link</a></td><td>{item.snippet.title}</td><td>{item.snippet.description}</td></tr>
		)}
		</table>
		</div>
		
	</div>);
    
  }

}

export default WithContext(SearchChannels);

/*
<source src="video.mp4" type="video/mp4"><!-- MP4 для Safari, IE9, iPhone, iPad, Android, и Windows Phone 7 -->
		  <source src="video.webm" type="video/webm"><!-- WebM/VP8 для Firefox4, Opera, и Chrome -->
		  <source src="video.ogv" type="video/ogg"><!-- Ogg/Vorbis для старых версий браузеров Firefox и Opera -->
		  <object data="video.swf" type="application/x-shockwave-flash"><!-- добавляем видеоконтент для устаревших браузеров, в которых нет поддержки элемента video -->
			<param name="movie" value="video.swf">
		  </object>*/