import React from "react";
import Snackbar from "../../assets/jss/material-dashboard-pro-react/components/Snackbar/Snackbar";

import "./style.css"


function Alert({message, isOpen, handlerClose, theme}) {
  return (
    <div className="custom-alert">
      <Snackbar
        fullWidth
        place="tl"
        color={theme}
        message={message}
        open={isOpen}
        closeNotification={() => handlerClose()}
        close
      />
    </div>
  );
}

// theme props colors
// info - blue color
// success - green color
// warning - yellow color
// danger - red color
// primary - violet  color
// rose - pink color
export default  Alert;