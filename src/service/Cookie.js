import * as cookies from '../classes/cookies.js';

export default {
  succsessLoginCookie(email) {
    cookies.set_cookie('email', email);
    cookies.set_cookie('isLoggedIn', '1');
  },

  logoutCookie() {
    cookies.set_cookie('email', 'Not logged in');
    cookies.set_cookie('isLoggedIn', '0');
    cookies.delete_cookie('PHPSESSID')
  },
};