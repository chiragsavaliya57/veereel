import React, {useState} from "react";
import {useDispatch, useSelector} from 'react-redux';
import {registration} from "../../reducer/auth/actions";

import Button from "../../components/Button"
import Input from "../../components/Input"
import SubHeaderEditable from "../../components/SubHeaderEditable"

import "./style.css"


function Registration() {
  const dispatch = useDispatch();
  const [editalbeTitle, setEditalbeTitle] = useState("Registration");
  const {error, isLoading, payload} = useSelector(state => state.auth.registration);
  const [userInfo,setUserInfo] = useState({
    email: '',
    password_1: '',
    password_2: ''
  });

  const handleChangeInputs = (type, value) => {
    setUserInfo({...userInfo, [type]: value})
  };

  const handleSubmit = (event) => {
    event.preventDefault();
    dispatch(registration(userInfo));
  };

  const updateTitle = (value) => {
    setEditalbeTitle(value)
  };

  return (
    <div className="registration-container">
      <SubHeaderEditable
        handleSave={updateTitle}
        initialValue={editalbeTitle}
        changeBlock={props => (
          <Input {...props}
                 className="editable_input"
                 onChange={m => props.handleChange(m || null)}/>)}>
        <h1>{editalbeTitle}</h1>
      </SubHeaderEditable>
      <div>
        <Input
          type="text" value={userInfo.email}
          onChange={(e) => handleChangeInputs('email', e)}
          placeholder={"Enter email"}
          labelText={"Email"}
        />
      </div>
      <div>
        <Input
          type="password"
          value={userInfo.password_1}
          onChange={(e) => handleChangeInputs('password_1', e)}
          placeholder={"Enter password"}
          labelText={"Password"}
        />
      </div>
      <div>
        <Input
          type="password"
          value={userInfo.password_2}
          onChange={(e) => handleChangeInputs('password_2', e)}
          placeholder={"Enter password"}
          labelText={"Password"}
        />
      </div>
      <div>
        <Button onClick={handleSubmit}>Registration</Button>
      </div>
      {error && <h2>{error.join(", ")}</h2>}
      {isLoading && <i>Loading...</i>}
      {payload && <i>Registration success...</i>}
    </div>

  );
}

export default Registration;