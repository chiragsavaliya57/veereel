import React from 'react';
import Icon from '@material-ui/core/Icon';
import { Link } from 'react-router-dom';

import AdminNavbar from '../../assets/jss/material-dashboard-pro-react/components/Navbars/AdminNavbar';
import './style.css';

function Header({brandText, rtlActive, sidebarMinimize, miniActive, handleDrawerToggle, subTitle, subTitleHref}) {
  return (
    <div className="admin-header">
      <AdminNavbar
        rtlActive={rtlActive}
        sidebarMinimize={sidebarMinimize}
        miniActive={miniActive}
        brandText={subTitle ?
          <React.Fragment>
            <div className="admin-header-title">{brandText}</div>
            <Link to={subTitleHref} className="admin-header-subtitle">
              <Icon>arrow_back</Icon>
              <span>{subTitle}</span>
            </Link>
          </React.Fragment>
          :
          <React.Fragment>
            <div className="admin-header-title">{brandText}</div>
          </React.Fragment>}
        handleDrawerToggle={handleDrawerToggle}
      />
    </div>
  );
}

export default React.memo(Header);