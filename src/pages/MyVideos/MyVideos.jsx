/*!

=========================================================
* Material Dashboard PRO React - v1.7.0
=========================================================

* Product Page: https://www.creative-tim.com/product/material-dashboard-pro-react
* Copyright 2019 Creative Tim (https://www.creative-tim.com)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
import React from "react";
import PropTypes from "prop-types";
// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";
import { connect } from 'react-redux';

// material-ui icons
import Assignment from "@material-ui/icons/Assignment";
import { MoreHoriz } from "@material-ui/icons";
import Close from "@material-ui/icons/Close";
import Remove from "@material-ui/icons/Remove";
import Add from "@material-ui/icons/Add";
import { ProgressBar } from "react-bootstrap";
import Tooltip from '@material-ui/core/Tooltip';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Divider from '@material-ui/core/Divider';
import FileCopyOutlinedIcon from '@material-ui/icons/FileCopyOutlined';
import EditOutlinedIcon from '@material-ui/icons/EditOutlined';
import DeleteOutlineOutlinedIcon from '@material-ui/icons/DeleteOutlineOutlined';

import { purple } from '@material-ui/core/colors';

// core components
import SnackbarContent from "components/Snackbar/SnackbarContent.jsx";
import GridContainer from "components/Grid/GridContainer.jsx";
import GridItem from "components/Grid/GridItem.jsx";
import Table from "components/Table/Table.jsx";
import Button from "components/CustomButtons/Button.jsx";
import Card from "components/Card/Card.jsx";
import CardBody from "components/Card/CardBody.jsx";
import CardIcon from "components/Card/CardIcon.jsx";
import CardHeader from "components/Card/CardHeader.jsx";

import extendedTablesStyle from "assets/jss/material-dashboard-pro-react/views/extendedTablesStyle.jsx";

import product1 from "assets/img/card-1.jpeg";
import product2 from "assets/img/card-2.jpeg";
import product3 from "assets/img/card-3.jpeg";
import './MyVideos.css';
import ReactPlayer from "react-player";

import CustomizedSnackbars from 'components/Snackbar/CustomizedSnackbars.js';
import { getVideo,getSession } from '../../reducer/adCreator/actions';
import { setNotificationMessage } from '../../reducer/adNotification/actions'

import CustomSelect from '../../components/CustomSelect/index';
import CustomDropdown from 'components/CustomDropdown/CustomDropdown.jsx';
import Icon from '@material-ui/core/Icon';

const HtmlTooltip = withStyles(theme => ({
  tooltip: {
    backgroundColor: '#f5f5f9',
    color: 'rgba(0, 0, 0, 0.87)',
    maxWidth: 220,
    fontSize: theme.typography.pxToRem(12),
    border: '1px solid #dadde9',
  },
}))(Tooltip);

class ExtendedTables extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      checked: [],
      isnotificationopen: false,
    };
    this.handleToggle = this.handleToggle.bind(this);
  }

  handleToggle(value) {
    const { checked } = this.state;
    const currentIndex = checked.indexOf(value);
    const newChecked = [...checked];

    if (currentIndex === -1) {
      newChecked.push(value);
    } else {
      newChecked.splice(currentIndex, 1);
    }

    this.setState({
      checked: newChecked
    });
  }

  componentDidMount() {
    this.props.getVideo();
    this.props.getSession();
    this.setState({ isnotificationopen: true });
    setTimeout(() => {
      this.saveNotification();
    }, 500);
  }

  saveNotification = () => {
    const { isnotificationopen } = this.state
    if (isnotificationopen) {
      this.props.setNotificationMessage('This is a notification with close button in My Video Tab.')
    }
  }

  render() {
    const { classes } = this.props;
    const { isnotificationopen } = this.state;

    const rowLen = this.props.getVideoResult.payload.length;


    return (
      <GridContainer>
        <GridItem xs={12}>
          <Card>
            <CustomizedSnackbars
              open={isnotificationopen}
              onclose={(val) => this.setState({ isnotificationopen: val })}
              message={"This is a notification with close button."}
              seconds={6000}
              type={"info"}
            />

            <br />
            <CardBody>

              { this.props.getSessionResult.payload.length != 0 ? (
                <Table
                  tableHead={[
                    "Preview",
                    "Name",
                    "Status",
                    "Created",
                    "",
                    ""
                  ]}
                  tableData={

                    this.props.getSessionResult.payload.map((item, index) => (
                      [
                        <div className={classes.imgContainer} key="key">
                          <ReactPlayer url={'https://anabasis.online/main/videos/'+(item.videos.length<=0 ? "":item.videos[item.videos.length-1].uid)+'.'+item.extension} width={200} height={100} controls={true} />
                        </div>,
                        <span key="key">
                          <a href="#jacket" className={classes.tdNameAnchor}>
                            {item.name}
                          </a>
                        </span>,
                        <span key="key">
                          {
                            item.videos.length<=0 ? <div></div>:
                            item.videos[item.videos.length-1].state=="finished" ?  <div className={classes.progressButtonCompleted}>
                                        <p className={classes.progressButtonPara}>Completed</p>
                                      </div> :  
                            item.videos[item.videos.length-1].state=="queued" ? <div className={classes.progressButtonDraft}>
                                        <p className={classes.progressButtonPara}>Queued</p>
                                      </div> :
                            item.videos[item.videos.length-1].state=="started" ? <div className={classes.progressButtonDraft}>
                                        <p className={classes.progressButtonPara}>Started</p>
                                      </div> :
                            <ProgressBar now={100} label={`Error`}/>
                          }
                        </span>,
                        <span key="key">
                          {item.created}
                        </span>,
                        <span key="key">
                          {/* <Button round className='shareButton'>Share</Button> */}
                          <CustomDropdown
                            dropup
                            buttonText="Share"
                            buttonColor={["info"]}
                            buttonProps={{
                              round: true
                            }}
                            dropdownList={[
                              <div className="pro-cover v-flex-block">
                                <div className="v-icon">
                                  <Icon>subscriptions</Icon>
                                </div>
                                <div className="v-text">
                                  <p className="v-upload-text">Upload to Youtube</p>
                                  <span className="v-premium-text">Upgrade to Premium</span>
                                </div>
                                <div className="v-icon">
                                  <Icon>lock</Icon>
                                </div>
                              </div>,
                              <div>
                                <div className="pro-cover">
                                  <div className="v-icon">
                                    <Icon>cloud_download</Icon>
                                  </div>
                                  <div className="v-text">
                                    <p className="v-upload-text">Upload to Youtube</p>
                                  </div>
                                </div>
                                <div className="pro-cover">
                                  <div className="v-icon">

                                  </div>
                                  <div className="v-text">
                                    <p className="v-label">1920x1080</p>
                                    <span className="v-premium-text">Upgrade to Premium</span>
                                  </div>
                                  <div className="v-icon">
                                    <Icon>lock</Icon>
                                  </div>
                                </div>
                              </div>,
                              <div className="pro-cover">
                                <div className="v-icon">

                                </div>
                                <div className="v-text">
                                  <p className="v-label">640x480</p>
                                </div>
                                <div className="v-icon">
                                  <Icon>get_app</Icon>
                                </div>
                              </div>
                            ]}
                          />
                        </span>,
                        <span key="key" className="actionButton">
                          <CustomDropdown
                            dropup
                            buttonColor={["defaultNoBackground"]}
                            buttonText={<MoreHoriz className={classes.videoAction} />}
                            buttonProps={{
                              round: true,
                              justIcon: true
                            }}
                            dropdownList={[
                              <div className="pro-cover">
                                <div className="v-action-text">
                                  <p className="marginBottomZ">Duplicate</p>
                                </div>
                                <div className="v-action-icon">
                                  <Icon>filter_noneedit</Icon>
                                </div>
                              </div>,
                              <div className="pro-cover">
                                <div className="v-action-text">
                                  <p className="marginBottomZ">Edit</p>
                                </div>
                                <div className="v-action-icon">
                                  <Icon>edit</Icon>
                                </div>
                              </div>,
                              <div className="pro-cover">
                                <div className="v-action-text">
                                  <p className="marginBottomZ">Delete</p>
                                </div>
                                <div className="v-action-icon">
                                  <Icon>delete</Icon>
                                </div>
                              </div>
                            ]}
                          />
                        </span>
                      ]
                    ))
                  }
                  tableShopping
                  customHeadCellClasses={[
                    classes.description,
                    classes.description,
                    classes.description,
                    classes.right,
                    classes.right,
                    classes.right
                  ]}
                  customHeadClassesForCells={[0, 2, 3, 4, 5, 6]}
                  customCellClasses={[
                    classes.tdName,
                    classes.progressCell,
                    classes.customFont,
                    classes.tdNumber,
                    classes.tdNumber + " " + classes.tdNumberAndButtonGroup,
                    classes.tdNumber
                  ]}
                  customClassesForCells={[1, 2, 3, 4, 5, 6]}
                />
              ) :
                (
                  <p className="no-data">No Data Found</p>
                )
              }
            </CardBody>
          </Card>
        </GridItem>
      </GridContainer>
    );
  }
}

ExtendedTables.propTypes = {
  classes: PropTypes.object
};

const mapStateToProps = ({ router: { location: { state: routerState } }, adCreator: { postVideoResult }, adCreator: { getVideoResult },adCreator: {getSessionResult} }) => ({ routerState, postVideoResult, getVideoResult,getSessionResult });

const mapDispatchToProps = dispatch => ({
  getVideo: () => dispatch(getVideo()),
  setNotificationMessage: message => dispatch(setNotificationMessage(message)),
  getSession: () => dispatch(getSession())
});

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(extendedTablesStyle)(ExtendedTables));
