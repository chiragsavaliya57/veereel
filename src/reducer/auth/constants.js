export const AUTH_START = 'auth/start';
export const AUTH_SUCCESS = 'auth/success';
export const AUTH_FAILED = 'auth/failed';

export const LOGOUT_START = 'logout/start';
export const LOGOUT_SUCCESS = 'logout/success';
export const LOGOUT_FAILED = 'logout/failed';

export const REGISTRATION_START = 'registration/start';
export const REGISTRATION_SUCCESS = 'registration/success';
export const REGISTRATION_FAILED = 'registration/failed';