import {
  GET_VIDEO,
  GET_VIDEO_SUCCESS,
  GET_VIDEO_FAILED,
  FIND_VIDEO,
  FIND_VIDEO_SUCCES,
  FIND_VIDEO_FAILED
} from "./constants";

export const getVideo = (params) => (dispatch, getState, { GetVideoInfoService }) => {
  dispatch({
    type: GET_VIDEO
  });

  return GetVideoInfoService.getVideo(params).then(payload => {
    dispatch({
      type: GET_VIDEO_SUCCESS,
      payload
    })
  }).catch(payload => {
    dispatch({
      type: GET_VIDEO_FAILED,
      payload
    })
  })
};


export const findVideo = (params) => (dispatch, getState, { GetVideoInfoService }) => {
  dispatch({
    type: FIND_VIDEO
  });

  return GetVideoInfoService.findVideo(params).then(payload => {
    dispatch({
      type: FIND_VIDEO_SUCCES,
      payload
    })
  }).catch(payload => {
    dispatch({
      type: FIND_VIDEO_FAILED,
      payload
    })
  })
};